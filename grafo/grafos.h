#ifndef _GRAFO_INCLUDE
#define _GRAFO_INCLUDE

#define _MAX_NOS_GRAFO 100
#define _MAX_ARCOS_GRAFO 200
#define NUMERO_CHAR_IN 100

#define NORTE_SUL	0
#define ESTE_OESTE	1
#define PLANO		2

typedef struct No {
	float x, y, z, largura, id;
}No;

typedef struct Arco {
	int noi, nof;
	float peso, largura;
}Arco;

extern No nos[];
extern Arco arcos[];
extern char nomePoi[NUMERO_CHAR_IN][_MAX_NOS_GRAFO];
extern int numNos, numArcos;

void addNo(No);
void deleteNo(int);
void imprimeNo(No);
void listNos();
No criaNo(float, float, float);

void addArco(Arco);
void deleteArco(int);
void imprimeArco(Arco);
void listArcos();
Arco criaArco(int, int, float, float);

void gravaGrafo();
void leGrafo();

#endif