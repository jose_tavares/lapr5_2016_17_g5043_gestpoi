imovel(i1001, a, 'Saude', 30).
imovel(i1002, b, 'Museu', 60).
imovel(i1003, f, 'Cultura', 45).
imovel(i1004, c, 'Musica' , 20).

%DISPONIBILIDADE IMOVEL (identificação, dia, hora, acessibilidade) 
%acessibilidade  (0 - não acessível; 1 - acessível). 

%Disponivel visita ao 'HOSPITAL S.JOAO' Segunda-feira das 14:00h as 18:30h e permite necessidades especiais.
disp_imovel(i1001, 2, 28, 1).		
disp_imovel(i1001, 2, 29, 1).
disp_imovel(i1001, 2, 30, 1).
disp_imovel(i1001, 2, 31, 1).
disp_imovel(i1001, 2, 32, 1).
disp_imovel(i1001, 2, 33, 1).
disp_imovel(i1001, 2, 34, 1).
disp_imovel(i1001, 2, 35, 1).
disp_imovel(i1001, 2, 36, 1).
disp_imovel(i1001, 2, 37, 1).

%Disponivel visita ao 'HOSPITAL S.JOAO' Terça-feira das 14:00h as 18:30h e permite necessidades especiais.
disp_imovel(i1001, 3, 28, 1).		
disp_imovel(i1001, 3, 29, 1).
disp_imovel(i1001, 3, 30, 1).
disp_imovel(i1001, 3, 31, 1).
disp_imovel(i1001, 3, 32, 1).
disp_imovel(i1001, 3, 33, 1).
disp_imovel(i1001, 3, 34, 1).
disp_imovel(i1001, 3, 35, 1).
disp_imovel(i1001, 3, 36, 1).
disp_imovel(i1001, 3, 37, 1).

%Disponivel visita ao 'HOSPITAL S.JOAO' Quarta-feira das 14:00h as 18:30h e permite necessidades especiais.
disp_imovel(i1001, 4, 28, 1).		
disp_imovel(i1001, 4, 29, 1).
disp_imovel(i1001, 4, 30, 1).
disp_imovel(i1001, 4, 31, 1).
disp_imovel(i1001, 4, 32, 1).
disp_imovel(i1001, 4, 33, 1).
disp_imovel(i1001, 4, 34, 1).
disp_imovel(i1001, 4, 35, 1).
disp_imovel(i1001, 4, 36, 1).
disp_imovel(i1001, 4, 37, 1).

%Disponivel visita ao 'HOSPITAL S.JOAO' Quinta-feira das 14:00h as 18:30h e permite necessidades especiais.
disp_imovel(i1001, 5, 28, 1).		
disp_imovel(i1001, 5, 29, 1).
disp_imovel(i1001, 5, 30, 1).
disp_imovel(i1001, 5, 31, 1).
disp_imovel(i1001, 5, 32, 1).
disp_imovel(i1001, 5, 33, 1).
disp_imovel(i1001, 5, 34, 1).
disp_imovel(i1001, 5, 35, 1).
disp_imovel(i1001, 5, 36, 1).
disp_imovel(i1001, 5, 37, 1).

%Disponivel visita ao 'HOSPITAL S.JOAO' Sexta-feira das 14:00h as 18:30h e permite necessidades especiais.
disp_imovel(i1001, 6, 28, 1).		
disp_imovel(i1001, 6, 29, 1).
disp_imovel(i1001, 6, 30, 1).
disp_imovel(i1001, 6, 31, 1).
disp_imovel(i1001, 6, 32, 1).
disp_imovel(i1001, 6, 33, 1).
disp_imovel(i1001, 6, 34, 1).
disp_imovel(i1001, 6, 35, 1).
disp_imovel(i1001, 6, 36, 1).
disp_imovel(i1001, 6, 37, 1).

%Disponivel visita ao 'HOSPITAL S.JOAO' Sabado das 09:00h as 12:30h e das 15:00 as 19:00 e permite necessidades especiais.
disp_imovel(i1001, 7, 18, 1).		
disp_imovel(i1001, 7, 19, 1).
disp_imovel(i1001, 7, 20, 1).
disp_imovel(i1001, 7, 21, 1).
disp_imovel(i1001, 7, 22, 1).
disp_imovel(i1001, 7, 23, 1).
disp_imovel(i1001, 7, 24, 1).
disp_imovel(i1001, 7, 25, 1).

disp_imovel(i1001, 7, 30, 1).		
disp_imovel(i1001, 7, 31, 1).
disp_imovel(i1001, 7, 32, 1).
disp_imovel(i1001, 7, 33, 1).
disp_imovel(i1001, 7, 34, 1).
disp_imovel(i1001, 7, 35, 1).
disp_imovel(i1001, 7, 36, 1).
disp_imovel(i1001, 7, 37, 1).
disp_imovel(i1001, 7, 38, 1).

%Disponivel visita ao 'PALACIO DA BOLSA' Terça-feira das 14:00h as 17:30h e não permite necessidades especiais.
disp_imovel(i1002, 3, 28, 0).		
disp_imovel(i1002, 3, 29, 0).
disp_imovel(i1002, 3, 30, 0).
disp_imovel(i1002, 3, 31, 0).
disp_imovel(i1002, 3, 32, 0).
disp_imovel(i1002, 3, 33, 0).
disp_imovel(i1002, 3, 34, 0).
disp_imovel(i1002, 3, 35, 0).

%Disponivel visita ao 'PALACIO DA BOLSA' Quarta-feira das 15:00h as 17:00h e não permite necessidades especiais.
disp_imovel(i1002, 4, 30, 0).		
disp_imovel(i1002, 4, 31, 0).
disp_imovel(i1002, 4, 32, 0).
disp_imovel(i1002, 4, 33, 0).
disp_imovel(i1002, 4, 34, 0).

%Disponivel visita ao 'PALACIO DA BOLSA' Quinta-feira das 16:30h as 18:30h e não permite necessidades especiais.
disp_imovel(i1002, 5, 33, 0).
disp_imovel(i1002, 5, 34, 0).
disp_imovel(i1002, 5, 35, 0).
disp_imovel(i1002, 5, 36, 0).
disp_imovel(i1002, 5, 37, 0).

%Disponivel visita ao 'CASA DA MUSICA' Terça-feira das 12:30h as 18:30h e permite necessidades especiais.
disp_imovel(i1003, 3, 25, 1).
disp_imovel(i1003, 3, 26, 1).
disp_imovel(i1003, 3, 27, 1).
disp_imovel(i1003, 3, 28, 1).
disp_imovel(i1003, 3, 29, 1).
disp_imovel(i1003, 3, 30, 1).
disp_imovel(i1003, 3, 31, 1).
disp_imovel(i1003, 3, 32, 1).
disp_imovel(i1003, 3, 33, 1).
disp_imovel(i1003, 3, 34, 1).
disp_imovel(i1003, 3, 35, 1).
disp_imovel(i1003, 3, 36, 1).
disp_imovel(i1003, 3, 37, 1).

%Disponivel visita ao 'CASA DA MUSICA' Sexta-feira das 12:30h as 18:30h e permite necessidades especiais.
disp_imovel(i1003, 6, 25, 1).
disp_imovel(i1003, 6, 26, 1).
disp_imovel(i1003, 6, 27, 1).
disp_imovel(i1003, 6, 28, 1).
disp_imovel(i1003, 6, 29, 1).
disp_imovel(i1003, 6, 30, 1).
disp_imovel(i1003, 6, 31, 1).
disp_imovel(i1003, 6, 32, 1).
disp_imovel(i1003, 6, 33, 1).
disp_imovel(i1003, 6, 34, 1).
disp_imovel(i1003, 6, 35, 1).
disp_imovel(i1003, 6, 36, 1).
disp_imovel(i1003, 6, 37, 1).

%Disponivel visita ao 'SE DO PORTO' Terça-feira das 14:00h as 18:00h e não permite necessidades especiais.
disp_imovel(i1004, 3, 28, 0).		
disp_imovel(i1004, 3, 29, 0).
disp_imovel(i1004, 3, 30, 0).
disp_imovel(i1004, 3, 31, 0).
disp_imovel(i1004, 3, 32, 0).
disp_imovel(i1004, 3, 33, 0).
disp_imovel(i1004, 3, 34, 0).
disp_imovel(i1004, 3, 35, 0).
disp_imovel(i1004, 3, 36, 0).

%Disponivel visita ao 'SE DO PORTO' Quarta-feira das 14:00h as 18:00h e não permite necessidades especiais.
disp_imovel(i1004, 4, 28, 0).		
disp_imovel(i1004, 4, 29, 0).
disp_imovel(i1004, 4, 30, 0).
disp_imovel(i1004, 4, 31, 0).
disp_imovel(i1004, 4, 32, 0).
disp_imovel(i1004, 4, 33, 0).
disp_imovel(i1004, 4, 34, 0).
disp_imovel(i1004, 4, 35, 0).
disp_imovel(i1004, 4, 36, 0).

%Disponivel visita ao 'SE DO PORTO' Quinta-feira das 14:00h as 18:00h e não permite necessidades especiais.
disp_imovel(i1004, 5, 28, 0).		
disp_imovel(i1004, 5, 29, 0).
disp_imovel(i1004, 5, 30, 0).
disp_imovel(i1004, 5, 31, 0).
disp_imovel(i1004, 5, 32, 0).
disp_imovel(i1004, 5, 33, 0).
disp_imovel(i1004, 5, 34, 0).
disp_imovel(i1004, 5, 35, 0).
disp_imovel(i1004, 5, 36, 0).

%Disponivel visita ao 'SE DO PORTO' Sexta-feira das 14:00h as 18:00h e não permite necessidades especiais.
disp_imovel(i1004, 6, 28, 0).		
disp_imovel(i1004, 6, 29, 0).
disp_imovel(i1004, 6, 30, 0).
disp_imovel(i1004, 6, 31, 0).
disp_imovel(i1004, 6, 32, 0).
disp_imovel(i1004, 6, 33, 0).
disp_imovel(i1004, 6, 34, 0).
disp_imovel(i1004, 6, 35, 0).
disp_imovel(i1004, 6, 36, 0).


%CRIAR TURISTA
turista(t1001, 'Filipe Silva').
turista(t1002, 'Paulo Costa').
turista(t1003, 'Jose Tavares').
turista(t1004, 'Miguel Barbosa').
turista(t1005, 'Gabriel Sousa').
turista(t1006, 'Telmo Marques').

%DISPONIBILIDADE TURISTA (identificação, dia, hora, acessibilidade)
%acessibilidade  (0 - sem necessidades especiais; 1 - com necessidades especiais).
 
%Disponibilidade do 'Filipe Silva' Segunda-feira das 17:00h as 20:30h com necessidades especiais e limite de 50km com 0 minutos a pe e 120 minutos de carro e um visita de 4H.
disp_turista(t1001, 2, 34, 1, 50, 0, 120, 4).		
disp_turista(t1001, 2, 35, 1, 50, 0, 120, 4).
disp_turista(t1001, 2, 36, 1, 50, 0, 120, 4).
disp_turista(t1001, 2, 37, 1, 50, 0, 120, 4).
disp_turista(t1001, 2, 38, 1, 50, 0, 120, 4).
disp_turista(t1001, 2, 39, 1, 50, 0, 120, 4).
disp_turista(t1001, 2, 40, 1, 50, 0, 120, 4).
disp_turista(t1001, 2, 41, 1, 50, 0, 120, 4).

%Disponibilidade do 'Filipe Silva' Quinta-feira das 14:00h as 18:30h com necessidades especiais e limite de 50km com 0 minutos a pe e 120 minutos de carro e uma visita de 4H.
disp_turista(t1001, 5, 28, 1, 50, 0, 120, 4).		
disp_turista(t1001, 5, 29, 1, 50, 0, 120, 4).
disp_turista(t1001, 5, 30, 1, 50, 0, 120, 4).
disp_turista(t1001, 5, 31, 1, 50, 0, 120, 4).
disp_turista(t1001, 5, 32, 1, 50, 0, 120, 4).
disp_turista(t1001, 5, 33, 1, 50, 0, 120, 4).
disp_turista(t1001, 5, 34, 1, 50, 0, 120, 4).
disp_turista(t1001, 5, 35, 1, 50, 0, 120, 4).
disp_turista(t1001, 5, 36, 1, 50, 0, 120, 4).
disp_turista(t1001, 5, 37, 1, 50, 0, 120, 4).

%Disponibilidade do 'Filipe Silva' Sabado das 10:00h as 14:00h com necessidades especiais e limite de 50km com 0 minutos a pe e 120 minutos de carro e uma Visita de 4H.
disp_turista(t1001, 7, 20, 1, 50, 0, 120, 4).		
disp_turista(t1001, 7, 21, 1, 50, 0, 120, 4).
disp_turista(t1001, 7, 22, 1, 50, 0, 120, 4).
disp_turista(t1001, 7, 23, 1, 50, 0, 120, 4).
disp_turista(t1001, 7, 24, 1, 50, 0, 120, 4).
disp_turista(t1001, 7, 25, 1, 50, 0, 120, 4).
disp_turista(t1001, 7, 26, 1, 50, 0, 120, 4).
disp_turista(t1001, 7, 27, 1, 50, 0, 120, 4).
disp_turista(t1001, 7, 28, 1, 50, 0, 120, 4).

%Disponibilidade do 'Paulo Costa' Terça-feira das 13:30h as 16:30h sem necessidades especiais e limite de 8km com 60 minutos a pe e 120 de carro e uma visita de 4H.
disp_turista(t1002, 3, 27, 0, 8, 60, 120, 4).
disp_turista(t1002, 3, 28, 0, 8, 60, 120, 4).
disp_turista(t1002, 3, 29, 0, 8, 60, 120, 4).
disp_turista(t1002, 3, 30, 0, 8, 60, 120, 4).
disp_turista(t1002, 3, 31, 0, 8, 60, 120, 4).
disp_turista(t1002, 3, 32, 0, 8, 60, 120, 4).
disp_turista(t1002, 3, 33, 0, 8, 60, 120, 4).

%Disponibilidade do 'Paulo Costa' Quarta-feira das 15:30h as 16:30h sem necessidades especiais e limite de 8km com 60 minutos a pe e 120 de carro e uma visita de 4H.
disp_turista(t1002, 4, 31, 0, 8, 60, 120, 4).
disp_turista(t1002, 4, 32, 0, 8, 60, 120, 4).
disp_turista(t1002, 4, 33, 0, 8, 60, 120, 4).

%Disponibilidade do 'Paulo Costa' Quinta-feira das 16:30h as 18:00h sem necessidades especiais e limite de 8km com 60 minutos a pe e 120 de carro e uma visita de 4H.
disp_turista(t1002, 5, 33, 0, 8, 60, 120, 4).
disp_turista(t1002, 5, 34, 0, 8, 60, 120, 4).
disp_turista(t1002, 5, 35, 0, 8, 60, 120, 4).
disp_turista(t1002, 5, 36, 0, 8, 60, 120, 4).

%Disponibilidade do 'José Tavares' Segunda-feira das 09:00h as 12:00h sem necessidades especiais e limite de 15km com 120 minutos a pe e 240 de carro e uma visita de 8H.
disp_turista(t1003, 2, 18, 0, 15, 120, 240, 8).
disp_turista(t1003, 2, 19, 0, 15, 120, 240, 8).
disp_turista(t1003, 2, 20, 0, 15, 120, 240, 8).
disp_turista(t1003, 2, 21, 0, 15, 120, 240, 8).
disp_turista(t1003, 2, 22, 0, 15, 120, 240, 8).
disp_turista(t1003, 2, 23, 0, 15, 120, 240, 8).
disp_turista(t1003, 2, 24, 0, 15, 120, 240, 8).

%Disponibilidade do 'José Tavares' Terça-feira das 13:00h as 15:30h sem necessidades especiais e limite de 15km com 120 minutos a pe e 240 de carro e visita de 8H.
disp_turista(t1003, 3, 26, 0, 15, 120, 240, 8).
disp_turista(t1003, 3, 27, 0, 15, 120, 240, 8).
disp_turista(t1003, 3, 28, 0, 15, 120, 240, 8).
disp_turista(t1003, 3, 29, 0, 15, 120, 240, 8).
disp_turista(t1003, 3, 30, 0, 15, 120, 240, 8).
disp_turista(t1003, 3, 31, 0, 15, 120, 240, 8).

%Disponibilidade do 'Miguel Barbosa' Segunda-feira das 09:00h as 12:00h sem necessidades especiais e limite de 80km com 180 minutos a pe e 0 de carro e visita de 8H.
disp_turista(t1004, 2, 18, 0, 80, 180, 0, 8).
disp_turista(t1004, 2, 19, 0, 80, 180, 0, 8).
disp_turista(t1004, 2, 20, 0, 80, 180, 0, 8).
disp_turista(t1004, 2, 21, 0, 80, 180, 0, 8).
disp_turista(t1004, 2, 22, 0, 80, 180, 0, 8).
disp_turista(t1004, 2, 23, 0, 80, 180, 0, 8).
disp_turista(t1004, 2, 24, 0, 80, 180, 0, 8).

%Disponibilidade do 'Miguel Barbosa' Terça-feira das 13:00h as 15:30h sem necessidades especiais e limite de 80km com 180 minutos a pe e 0 de carro e visita de 8H.
disp_turista(t1004, 3, 26, 0, 80, 180, 0, 8).
disp_turista(t1004, 3, 27, 0, 80, 180, 0, 8).
disp_turista(t1004, 3, 28, 0, 80, 180, 0, 8).
disp_turista(t1004, 3, 29, 0, 80, 180, 0, 8).
disp_turista(t1004, 3, 30, 0, 80, 180, 0, 8).
disp_turista(t1004, 3, 31, 0, 80, 180, 0, 8).

%Disponibilidade do 'Gabriel Sousa' Segunda-feira das 13:30h as 18:00h sem necessidades especiais e limite de 80km com 80 minutos a pe e 120 de carro e uma visita de 4H.
disp_turista(t1005, 2, 27, 0, 80, 60, 120, 4).
disp_turista(t1005, 2, 28, 0, 80, 60, 120, 4).
disp_turista(t1005, 2, 29, 0, 80, 60, 120, 4).
disp_turista(t1005, 2, 30, 0, 80, 60, 120, 4).
disp_turista(t1005, 2, 31, 0, 80, 60, 120, 4).
disp_turista(t1005, 2, 32, 0, 80, 60, 120, 4).
disp_turista(t1005, 2, 33, 0, 80, 60, 120, 4).
disp_turista(t1005, 2, 34, 0, 80, 60, 120, 4).
disp_turista(t1005, 2, 35, 0, 80, 60, 120, 4).
disp_turista(t1005, 2, 36, 0, 80, 60, 120, 4).

%Disponibilidade do 'Gabriel Sousa' Terça-feira das 13:30h as 18:00h sem necessidades especiais e limite de 80km com 60 minutos a pe e 120 de carro e uma visita de 4H.
disp_turista(t1005, 3, 27, 0, 80, 60, 120, 4).
disp_turista(t1005, 3, 28, 0, 80, 60, 120, 4).
disp_turista(t1005, 3, 29, 0, 80, 60, 120, 4).
disp_turista(t1005, 3, 30, 0, 80, 60, 120, 4).
disp_turista(t1005, 3, 31, 0, 80, 60, 120, 4).
disp_turista(t1005, 3, 32, 0, 80, 60, 120, 4).
disp_turista(t1005, 3, 33, 0, 80, 60, 120, 4).
disp_turista(t1005, 3, 34, 0, 80, 60, 120, 4).
disp_turista(t1005, 3, 35, 0, 80, 60, 120, 4).
disp_turista(t1005, 3, 36, 0, 80, 60, 120, 4).

%Disponibilidade do 'Gabriel Sousa' Quarta-feira das 13:30h as 18:00h sem necessidades especiais e limite de 80km com 60 minutos a pe e 120 de carro e uma visita de 4H.
disp_turista(t1005, 4, 27, 0, 80, 60, 120, 4).
disp_turista(t1005, 4, 28, 0, 80, 60, 120, 4).
disp_turista(t1005, 4, 29, 0, 80, 60, 120, 4).
disp_turista(t1005, 4, 30, 0, 80, 60, 120, 4).
disp_turista(t1005, 4, 31, 0, 80, 60, 120, 4).
disp_turista(t1005, 4, 32, 0, 80, 60, 120, 4).
disp_turista(t1005, 4, 33, 0, 80, 60, 120, 4).
disp_turista(t1005, 4, 34, 0, 80, 60, 120, 4).
disp_turista(t1005, 4, 35, 0, 80, 60, 120, 4).
disp_turista(t1005, 4, 36, 0, 80, 60, 120, 4).

%Disponibilidade do 'Gabriel Sousa' Quinta-feira das 13:30h as 18:00h sem necessidades especiais e limite de 80km com 60 minutos a pe e 120 de carro e uma visita de 4H.
disp_turista(t1005, 5, 27, 0, 80, 60, 120, 4).
disp_turista(t1005, 5, 28, 0, 80, 60, 120, 4).
disp_turista(t1005, 5, 29, 0, 80, 60, 120, 4).
disp_turista(t1005, 5, 30, 0, 80, 60, 120, 4).
disp_turista(t1005, 5, 31, 0, 80, 60, 120, 4).
disp_turista(t1005, 5, 32, 0, 80, 60, 120, 4).
disp_turista(t1005, 5, 33, 0, 80, 60, 120, 4).
disp_turista(t1005, 5, 34, 0, 80, 60, 120, 4).
disp_turista(t1005, 5, 35, 0, 80, 60, 120, 4).
disp_turista(t1005, 5, 36, 0, 80, 60, 120, 4).

%Disponibilidade do 'Gabriel Sousa' Sexta-feira das 13:30h as 18:00h sem necessidades especiais e limite de 80km com 60 minutos a pe e 120 de carro e uma visita de 4H.
disp_turista(t1005, 6, 27, 0, 80, 60, 120, 4).
disp_turista(t1005, 6, 28, 0, 80, 60, 120, 4).
disp_turista(t1005, 6, 29, 0, 80, 60, 120, 4).
disp_turista(t1005, 6, 30, 0, 80, 60, 120, 4).
disp_turista(t1005, 6, 31, 0, 80, 60, 120, 4).
disp_turista(t1005, 6, 32, 0, 80, 60, 120, 4).
disp_turista(t1005, 6, 33, 0, 80, 60, 120, 4).
disp_turista(t1005, 6, 34, 0, 80, 60, 120, 4).
disp_turista(t1005, 6, 35, 0, 80, 60, 120, 4).
disp_turista(t1005, 6, 36, 0, 80, 60, 120, 4).

%Disponibilidade do 'Telmo Marques' Segunda-feira das 13:30h as 18:00h sem necessidades especiais e limite de 80km com 80 minutos a pe e 120 de carro e uma visita de 4H.
disp_turista(t1006, 2, 27, 0, 80, 60, 120, 1).
disp_turista(t1006, 2, 28, 0, 80, 60, 120, 1).
disp_turista(t1006, 2, 29, 0, 80, 60, 120, 1).
disp_turista(t1006, 2, 30, 0, 80, 60, 120, 1).
disp_turista(t1006, 2, 31, 0, 80, 60, 120, 1).
disp_turista(t1006, 2, 32, 0, 80, 60, 120, 1).
disp_turista(t1006, 2, 33, 0, 80, 60, 120, 1).
disp_turista(t1006, 2, 34, 0, 80, 60, 120, 1).
disp_turista(t1006, 2, 35, 0, 80, 60, 120, 1).
disp_turista(t1006, 2, 36, 0, 80, 60, 120, 1).

%Disponibilidade do 'Telmo Marques' Terça-feira das 13:30h as 18:00h sem necessidades especiais e limite de 80km com 60 minutos a pe e 120 de carro e uma visita de 4H.
disp_turista(t1006, 3, 27, 0, 80, 60, 120, 1).
disp_turista(t1006, 3, 28, 0, 80, 60, 120, 1).
disp_turista(t1006, 3, 29, 0, 80, 60, 120, 1).
disp_turista(t1006, 3, 30, 0, 80, 60, 120, 1).
disp_turista(t1006, 3, 31, 0, 80, 60, 120, 1).
disp_turista(t1006, 3, 32, 0, 80, 60, 120, 1).
disp_turista(t1006, 3, 33, 0, 80, 60, 120, 1).
disp_turista(t1006, 3, 34, 0, 80, 60, 120, 1).
disp_turista(t1006, 3, 35, 0, 80, 60, 120, 1).
disp_turista(t1006, 3, 36, 0, 80, 60, 120, 1).

%Disponibilidade do 'Telmo Marques' Quarta-feira das 13:30h as 18:00h sem necessidades especiais e limite de 80km com 60 minutos a pe e 120 de carro e uma visita de 4H.
disp_turista(t1006, 4, 27, 0, 80, 60, 120, 1).
disp_turista(t1006, 4, 28, 0, 80, 60, 120, 1).
disp_turista(t1006, 4, 29, 0, 80, 60, 120, 1).
disp_turista(t1006, 4, 30, 0, 80, 60, 120, 1).
disp_turista(t1006, 4, 31, 0, 80, 60, 120, 1).
disp_turista(t1006, 4, 32, 0, 80, 60, 120, 1).
disp_turista(t1006, 4, 33, 0, 80, 60, 120, 1).
disp_turista(t1006, 4, 34, 0, 80, 60, 120, 1).
disp_turista(t1006, 4, 35, 0, 80, 60, 120, 1).
disp_turista(t1006, 4, 36, 0, 80, 60, 120, 1).

%Disponibilidade do 'Telmo Marques' Quinta-feira das 13:30h as 18:00h sem necessidades especiais e limite de 80km com 60 minutos a pe e 120 de carro e uma visita de 4H.
disp_turista(t1006, 5, 27, 0, 80, 60, 120, 1).
disp_turista(t1006, 5, 28, 0, 80, 60, 120, 1).
disp_turista(t1006, 5, 29, 0, 80, 60, 120, 1).
disp_turista(t1006, 5, 30, 0, 80, 60, 120, 1).
disp_turista(t1006, 5, 31, 0, 80, 60, 120, 1).
disp_turista(t1006, 5, 32, 0, 80, 60, 120, 1).
disp_turista(t1006, 5, 33, 0, 80, 60, 120, 1).
disp_turista(t1006, 5, 34, 0, 80, 60, 120, 1).
disp_turista(t1006, 5, 35, 0, 80, 60, 120, 1).
disp_turista(t1006, 5, 36, 0, 80, 60, 120, 1).

%Disponibilidade do 'Telmo Marques' Sexta-feira das 13:30h as 18:00h sem necessidades especiais e limite de 80km com 60 minutos a pe e 120 de carro e uma visita de 4H.
disp_turista(t1006, 6, 27, 0, 80, 60, 120, 1).
disp_turista(t1006, 6, 28, 0, 80, 60, 120, 1).
disp_turista(t1006, 6, 29, 0, 80, 60, 120, 1).
disp_turista(t1006, 6, 30, 0, 80, 60, 120, 1).
disp_turista(t1006, 6, 31, 0, 80, 60, 120, 1).
disp_turista(t1006, 6, 32, 0, 80, 60, 120, 1).
disp_turista(t1006, 6, 33, 0, 80, 60, 120, 1).
disp_turista(t1006, 6, 34, 0, 80, 60, 120, 1).
disp_turista(t1006, 6, 35, 0, 80, 60, 120, 1).
disp_turista(t1006, 6, 36, 0, 80, 60, 120, 1).


%VISITAS AOS IMOVEIS (POI'S)
visita_imovel(t1001, [i1001, i1002], i1001).
visita_imovel(t1002, [i1002, i1003], i1002).
visita_imovel(t1003, [i1001, i1002, i1003], i1001).
visita_imovel(t1004, [i1001, i1002, i1003], i1001).
visita_imovel(t1005, [i1002, i1004], i1001).
visita_imovel(t1006, [i1002, i1004], i1001).


