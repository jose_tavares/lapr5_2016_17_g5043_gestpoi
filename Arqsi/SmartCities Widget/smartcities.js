//globle variables
var widget;
var sensores;
var facetas;

function sensoresResponseHandler(response, args) {
    var sensors = response.getElementsByTagName("sensor");
    for (var i = 0; i < sensors.length; i++) {
        var sensor = sensors[i];
        var name = sensor.getElementsByTagName("nome")[0].firstChild.nodeValue;
        var rButton = document.createElement("input");

        var atributes = [
            ["type", "radio"],
            ["name", "sensor"],
            ["id", name],
            ["onchange", "getSensorAttributes(this)"]
        ];
        setAttributes(rButton, atributes);

        var rButtonName = document.createElement("widget");
        rButtonName.setAttribute("class", "tooltip");
        rButtonName.appendChild(document.createTextNode(name));
        var rButtonToolTip = document.createElement("span");
        rButtonToolTip.setAttribute("class", "tooltiptext");
        rButtonToolTip.appendChild(document.createTextNode(sensor.getElementsByTagName("descricao")[0].firstChild.nodeValue));
        rButtonName.appendChild(rButtonToolTip);

        sensores.appendChild(rButton);
        sensores.appendChild(rButtonName);
        sensores.appendChild(document.createElement("br"));
    }
}

function getSensorAttributes(rButton) {
    clearFacetas();
    request("GET", "http://phpdev2.dei.isep.ipp.pt/~arqsi/smartcity/facetasByNomeSensor.php", "?sensor=" + rButton.id, "XML", sensoresAttributesStateHandler, null);
}

function sensoresAttributesStateHandler(response, args) {
    var idSensor = getSelectedSensorID();
    var attributes = response.getElementsByTagName("Faceta");
    for (var i = 0; i < attributes.length; i++) {
        var attr = attributes[i];
        var container = document.createElement("widget");
        var element = document.createElement("widget");
        element.setAttribute("style", "display:none");

        var nome = attr.getElementsByTagName("Nome")[0].firstChild.nodeValue;
        var grandeza = attr.getElementsByTagName("Grandeza")[0].firstChild.nodeValue;
        var tipo = attr.getElementsByTagName("Tipo")[0].firstChild.nodeValue;
        var semantica = attr.getElementsByTagName("Semantica")[0].firstChild.nodeValue;
        var campoBD = attr.getElementsByTagName("CampoBD")[0].firstChild.nodeValue;
        var checkbox = document.createElement("input");

        var atributes = [
            ["type", "checkbox"],
            ["name", "faceta"],
            ["id", attr.getElementsByTagName("CampoBD")[0].firstChild.nodeValue],
            ["onchange", "triggerFaceta(this)"]
        ];
        setAttributes(checkbox, atributes);

        container.appendChild(checkbox);
        container.appendChild(document.createTextNode(nome))
        container.appendChild(element);

        if (tipo == "data") {
            if (grandeza != "contínuo") {
                var timeForm = createTimeForm("   Dia:  ");
                request("GET", "http://phpdev2.dei.isep.ipp.pt/~arqsi/smartcity/minFaceta.php", "?sensor=" + idSensor + "&facetaCont=" + campoBD, "JSON", minRequest, timeForm);
                request("GET", "http://phpdev2.dei.isep.ipp.pt/~arqsi/smartcity/maxFaceta.php", "?sensor=" + idSensor + "&facetaCont=" + campoBD, "JSON", maxRequest, timeForm);
                element.appendChild(timeForm);
            } else {
                var timeForm = createTimeForm("   De:   ");
                var timeForm2 = createTimeForm("   Ate:  ");
                request("GET", "http://phpdev2.dei.isep.ipp.pt/~arqsi/smartcity/minFaceta.php", "?sensor=" + idSensor + "&facetaCont=" + campoBD, "JSON", minRequest, [timeForm, timeForm2]);
                request("GET", "http://phpdev2.dei.isep.ipp.pt/~arqsi/smartcity/maxFaceta.php", "?sensor=" + idSensor + "&facetaCont=" + campoBD, "JSON", maxRequest, [timeForm, timeForm2]);
                element.appendChild(timeForm);
                element.appendChild(timeForm2);
            }
        }

        if (tipo == "hora") {
            if (grandeza != "contínuo") {
                var hourForm = createHourForm()("   Hora:  ");
                request("GET", "http://phpdev2.dei.isep.ipp.pt/~arqsi/smartcity/minFaceta.php", "?sensor=" + idSensor + "&facetaCont=" + campoBD, "JSON", minRequest, hourForm);
                request("GET", "http://phpdev2.dei.isep.ipp.pt/~arqsi/smartcity/maxFaceta.php", "?sensor=" + idSensor + "&facetaCont=" + campoBD, "JSON", maxRequest, hourForm);
                element.appendChild(hourForm);
            } else {
                var hourForm = createHourForm("   De:   ");
                var hourForm2 = createHourForm("   Ate:  ");
                request("GET", "http://phpdev2.dei.isep.ipp.pt/~arqsi/smartcity/minFaceta.php", "?sensor=" + idSensor + "&facetaCont=" + campoBD, "JSON", minRequest, [hourForm, hourForm2]);
                request("GET", "http://phpdev2.dei.isep.ipp.pt/~arqsi/smartcity/maxFaceta.php", "?sensor=" + idSensor + "&facetaCont=" + campoBD, "JSON", maxRequest, [hourForm, hourForm2]);
                element.appendChild(hourForm);
                element.appendChild(hourForm2);
            }
        }

        if (tipo == "numérico") {
            switch (semantica) {
                case "temperatura":
                    if (grandeza != "contínuo") {
                        var sliderForm = createSliderForm("   Temp:  ");
                        request("GET", "http://phpdev2.dei.isep.ipp.pt/~arqsi/smartcity/minFaceta.php", "?sensor=" + idSensor + "&facetaCont=Temp", "JSON", minRequestAdjusting, sliderForm);
                        request("GET", "http://phpdev2.dei.isep.ipp.pt/~arqsi/smartcity/maxFaceta.php", "?sensor=" + idSensor + "&facetaCont=Temp", "JSON", maxRequest, sliderForm);
                        element.appendChild(sliderForm);
                    } else {
                        var sliderForm = createSliderForm("   De:   ");
                        var sliderForm2 = createSliderForm("   Ate:  ");
                        request("GET", "http://phpdev2.dei.isep.ipp.pt/~arqsi/smartcity/minFaceta.php", "?sensor=" + idSensor + "&facetaCont=Temp", "JSON", minRequestAdjusting, [sliderForm, sliderForm2]);
                        request("GET", "http://phpdev2.dei.isep.ipp.pt/~arqsi/smartcity/maxFaceta.php", "?sensor=" + idSensor + "&facetaCont=Temp", "JSON", maxRequest, [sliderForm, sliderForm2]);
                        element.appendChild(sliderForm);
                        element.appendChild(sliderForm2);
                    }
                    break;
                case "monetário":
                    if (grandeza != "contínuo") {
                        var simpleNumberForm = createSimpleNumberForm("   Preco:  ", 0.01);
                        request("GET", "http://phpdev2.dei.isep.ipp.pt/~arqsi/smartcity/minFaceta.php", "?sensor=" + idSensor + "&facetaCont=" + campoBD, "JSON", minRequest, simpleNumberForm);
                        request("GET", "http://phpdev2.dei.isep.ipp.pt/~arqsi/smartcity/maxFaceta.php", "?sensor=" + idSensor + "&facetaCont=" + campoBD, "JSON", maxRequest, simpleNumberForm);
                        element.appendChild(simpleNumberForm);
                    } else {
                        var simpleNumberForm = createSimpleNumberForm("   Min:   ", 0.01);
                        var simpleNumberForm2 = createSimpleNumberForm("   Max:  ", 0.01);
                        request("GET", "http://phpdev2.dei.isep.ipp.pt/~arqsi/smartcity/minFaceta.php", "?sensor=" + idSensor + "&facetaCont=" + campoBD, "JSON", minRequest, [simpleNumberForm, simpleNumberForm2]);
                        request("GET", "http://phpdev2.dei.isep.ipp.pt/~arqsi/smartcity/maxFaceta.php", "?sensor=" + idSensor + "&facetaCont=" + campoBD, "JSON", maxRequest, [simpleNumberForm, simpleNumberForm2]);
                        element.appendChild(simpleNumberForm);
                        element.appendChild(simpleNumberForm2);
                    }
                    break;
                case "quantidade":
                    if (grandeza != "contínuo") {
                        var simpleNumberForm = createSimpleNumberForm("   Valor:  ", 1);
                        request("GET", "http://phpdev2.dei.isep.ipp.pt/~arqsi/smartcity/minFaceta.php", "?sensor=" + idSensor + "&facetaCont=" + campoBD, "JSON", minRequest, simpleNumberForm);
                        request("GET", "http://phpdev2.dei.isep.ipp.pt/~arqsi/smartcity/maxFaceta.php", "?sensor=" + idSensor + "&facetaCont=" + campoBD, "JSON", maxRequest, simpleNumberForm);
                        element.appendChild(simpleNumberForm);
                    } else {
                        var simpleNumberForm = createSimpleNumberForm("   Min:   ", 1);
                        var simpleNumberForm2 = createSimpleNumberForm("   Max:  ", 1);
                        request("GET", "http://phpdev2.dei.isep.ipp.pt/~arqsi/smartcity/minFaceta.php", "?sensor=" + idSensor + "&facetaCont=" + campoBD, "JSON", minRequest, [simpleNumberForm, simpleNumberForm2]);
                        request("GET", "http://phpdev2.dei.isep.ipp.pt/~arqsi/smartcity/maxFaceta.php", "?sensor=" + idSensor + "&facetaCont=" + campoBD, "JSON", maxRequest, [simpleNumberForm, simpleNumberForm2]);
                        element.appendChild(simpleNumberForm);
                        element.appendChild(simpleNumberForm2);
                    }
                    break;
                case "coordenadas":
                    if (grandeza != "contínuo") {
                        var sliderForm = createSliderForm("   Coordenadas:  ");
                        request("GET", "http://phpdev2.dei.isep.ipp.pt/~arqsi/smartcity/minFaceta.php", "?sensor=" + idSensor + "&facetaCont=" + campoBD, "JSON", minRequestAdjusting, sliderForm);
                        request("GET", "http://phpdev2.dei.isep.ipp.pt/~arqsi/smartcity/maxFaceta.php", "?sensor=" + idSensor + "&facetaCont=" + campoBD, "JSON", maxRequest, sliderForm);
                        element.appendChild(sliderForm);
                    } else {
                        var sliderForm = createSliderForm("   De:   ");
                        var sliderForm2 = createSliderForm("   Ate:  ");
                        request("GET", "http://phpdev2.dei.isep.ipp.pt/~arqsi/smartcity/minFaceta.php", "?sensor=" + idSensor + "&facetaCont=" + campoBD, "JSON", minRequestAdjusting, [sliderForm, sliderForm2]);
                        request("GET", "http://phpdev2.dei.isep.ipp.pt/~arqsi/smartcity/maxFaceta.php", "?sensor=" + idSensor + "&facetaCont=" + campoBD, "JSON", maxRequest, [sliderForm, sliderForm2]);
                        element.appendChild(sliderForm);
                        element.appendChild(sliderForm2);
                    }
                    break;
                default:
                    if (grandeza != "contínuo") {
                        var simpleNumberForm = createSimpleNumberForm("   " + nome + ":  ", 0.0001);
                        request("GET", "http://phpdev2.dei.isep.ipp.pt/~arqsi/smartcity/minFaceta.php", "?sensor=" + idSensor + "&facetaCont=" + campoBD, "JSON", minRequest, simpleNumberForm);
                        request("GET", "http://phpdev2.dei.isep.ipp.pt/~arqsi/smartcity/maxFaceta.php", "?sensor=" + idSensor + "&facetaCont=" + campoBD, "JSON", maxRequest, simpleNumberForm);
                        element.appendChild(simpleNumberForm);
                    } else {
                        var simpleNumberForm = createSimpleNumberForm("   Min:   ", 0.0001);
                        var simpleNumberForm2 = createSimpleNumberForm("   Max:  ", 0.0001);
                        request("GET", "http://phpdev2.dei.isep.ipp.pt/~arqsi/smartcity/minFaceta.php", "?sensor=" + idSensor + "&facetaCont=" + campoBD, "JSON", minRequest, [simpleNumberForm, simpleNumberForm2]);
                        request("GET", "http://phpdev2.dei.isep.ipp.pt/~arqsi/smartcity/maxFaceta.php", "?sensor=" + idSensor + "&facetaCont=" + campoBD, "JSON", maxRequest, [simpleNumberForm, simpleNumberForm2]);
                        element.appendChild(simpleNumberForm);
                        element.appendChild(simpleNumberForm2);
                    }
                    break;
            }
        }
        if (tipo == "alfanumérico" || tipo == "url") {
            switch (semantica) {
                case "coordenadas":
                    if (grandeza != "contínuo") {
                        var sliderForm = createSliderForm()("   GPS:  ");
                        request("GET", "http://phpdev2.dei.isep.ipp.pt/~arqsi/smartcity/minFaceta.php", "?sensor=" + idSensor + "&facetaCont=" + campoBD, "JSON", minRequestAdjusting, sliderForm);
                        request("GET", "http://phpdev2.dei.isep.ipp.pt/~arqsi/smartcity/maxFaceta.php", "?sensor=" + idSensor + "&facetaCont=" + campoBD, "JSON", maxRequest, sliderForm);
                        element.appendChild(sliderForm);
                    } else {
                        var sliderForm = createSliderForm("   De-N:");
                        var sliderForm2 = createSliderForm("   Ate-N:");
                        var sliderForm3 = createSliderForm("   De-W:");
                        var sliderForm4 = createSliderForm("   Ate:W");
                        request("GET", "http://phpdev2.dei.isep.ipp.pt/~arqsi/smartcity/minFaceta.php", "?sensor=" + idSensor + "&facetaCont=" + campoBD, "JSON", minRequestGPS, [sliderForm, sliderForm2, sliderForm3, sliderForm4]);
                        request("GET", "http://phpdev2.dei.isep.ipp.pt/~arqsi/smartcity/maxFaceta.php", "?sensor=" + idSensor + "&facetaCont=" + campoBD, "JSON", maxRequestGPS, [sliderForm, sliderForm2, sliderForm3, sliderForm4]);
                        element.appendChild(sliderForm);
                        element.appendChild(sliderForm2);
                        element.appendChild(sliderForm3);
                        element.appendChild(sliderForm4);
                    }
                    break;
                default:
                    request("GET", "http://phpdev2.dei.isep.ipp.pt/~arqsi/smartcity/valoresFacetadoSensor.php", "?sensor=" + idSensor + "&faceta=" + campoBD, "JSON", localizationRequest, element);
                    break;
            }
        }

        facetas.appendChild(container);
        facetas.appendChild(document.createElement("br"));
    }

    addButtons();
}

function minRequest(response, form) {
    var min = response.min.replace(",", ".");
    for (var i = 0; i < form.length; i++) {
        form[i].childNodes[1].min = min;
        if (form[i].id == "Temp") {
            form[i].childNodes[1].value = min;
            form[i].childNodes[2].nodeValue = min;
        }
    }
}

function minRequestGPS(response, form) {
    for (var i = 0; i < 2; i++) {
        var splitted = response.min.split(";")[0];
        splitted = splitted.replace(",", ".");
        var string = splitted.substring(1, splitted.length);
        form[i].childNodes[1].min = string;
        form[i].childNodes[1].value = string;
        form[i].childNodes[2].nodeValue = string;
    }

    for (var i = 2; i < 4; i++) {
        var splitted = response.min.split(";")[1];
        splitted = splitted.replace(",", ".");
        var string = splitted.substring(1, splitted.length);
        form[i].childNodes[1].min = string;
        form[i].childNodes[1].value = string;
        form[i].childNodes[2].nodeValue = string;
    }


}
function maxRequestGPS(response, form) {
    for (var i = 0; i < 2; i++) {
        var splitted = response.max.split(";")[0];
        splitted = splitted.replace(",", ".");
        var string = splitted.substring(1, splitted.length);
        form[i].childNodes[1].max = string;
    }

    for (var i = 2; i < 4; i++) {
        var splitted = response.max.split(";")[1];
        splitted = splitted.replace(",", ".");
        var string = splitted.substring(1, splitted.length);
        form[i].childNodes[1].max = string;
    }


}
function minRequestAdjusting(response, form) {
    var min = response.min.replace(",", ".");
    for (var i = 0; i < form.length; i++) {
        form[i].childNodes[1].min = min;
        form[i].childNodes[1].value = min;
        form[i].childNodes[2].nodeValue = min;
    }
}

function maxRequest(response, form) {
    var max = response.max.replace(",", ".");
    for (var i = 0; i < form.length; i++) {
        form[i].childNodes[1].max = max;
    }
}

function localizationRequest(response, element) {
    for (var i = 0; i < response.length; i++) {
        var checkbox = document.createElement("input");
        checkbox.setAttribute("type", "checkbox");
        checkbox.setAttribute("name", response[i]);
        element.appendChild(document.createElement("br"));
        element.appendChild(document.createTextNode("   "));
        element.appendChild(checkbox);
        element.appendChild(document.createTextNode(response[i]));
    }
}

function showValue(newValue) {
    newValue.nextSibling.nodeValue = newValue.value;
}

function addButtons() {
    addButtonsF("validate()");
}

function getSelectedSensorID() {
    for (var i = 0; i < sensores.childNodes.length; i++) {
        var sensor = sensores.childNodes[i];
        if (sensor.checked) {
            return sensor.id;
        }
    }
}

function validate() {
    clearResults();
    dataTreatment("?sensor=" + getSelectedSensorID() + "&", 0);
}

function dataTreatment(params, i) {
    var facetas = document.getElementById("facetas");
    if (i < facetas.childElementCount - 2) {
        var child = facetas.childNodes[i];
        if (child.firstChild.checked) { //Check if checkbox is selected
            var container = child.childNodes[2].getElementsByTagName("input");
            var values;
            //Discrete
            if (container[0].type == "checkbox") {
                var flag = true;
                values = child.firstChild.id + "=[";
                for (var j = 0; j < container.length; j++) {
                    if (container[j].checked) {
                        if (!flag) {
                            values += ",";
                        }
                        values += container[j].name;
                        flag = false;
                    }
                }
                values += "]";
                if (flag) {
                    window.alert("Nao foi selectionado " + child.childNodes[1].nodeValue + ". Erro!");
                    return;
                }
                params += values + "&";
                dataTreatment(params, i + 2);
            }
            //Continuous
            else {
                if (!(container[0].type == "time" || container[0].type == "date")) {
                    values = [container.length];
                    for (var j = 0; j < container.length; j++) {
                        var min = parseFloat(container[j].min);
                        var max = parseFloat(container[j].max);
                        var outFlag = true;
                        var value = parseFloat(container[j].value);
                        if (value == "" || value < min || value > max) {
                            window.alert("Valor inválido para " + child.childNodes[1].nodeValue + ". Erro!");
                            return;
                        }
                        values[j] = value;
                        if (j % 2 == 1) {
                            if (values[j - 1] > values[j]) {
                                window.alert("Valores de Minimo e Máximo incoerentes.");
                                return;
                            }
                        }
                    }

                    var idSensor = getSelectedSensorID();
                    var string = "" + child.firstChild.id;
                    if (child.childNodes[0].id == "GPS") {
                        request("GET", "http://phpdev2.dei.isep.ipp.pt/~arqsi/smartcity/valoresFacetadoSensor.php",
                                "?sensor=" + idSensor + "&faceta=" + child.firstChild.id,
                                "JSON", filterStuffGPS, [values, child.firstChild.id + "", params, i]);
                    } else {
                        request("GET", "http://phpdev2.dei.isep.ipp.pt/~arqsi/smartcity/valoresFacetadoSensor.php",
                                "?sensor=" + idSensor + "&faceta=" + child.firstChild.id,
                                "JSON", filterStuffNumber, [values, child.firstChild.id + "", params, i]);
                    }
                } else {
                    values = [container.length];
                    for (var j = 0; j < container.length; j++) {
                        var outFlag = true;
                        var value = container[j].value;

                        if (container[0].type == "time") {
                            if (value.split(":").length < 3) {
                                value += ":00";
                            }
                        }

                        if (value == "" || value < container[j].min || value > container[j].max) {
                            window.alert("Valor inválido para " + child.childNodes[1].nodeValue + ". Erro!");
                            return;
                        }
                        values[j] = value;
                        if (j % 2 == 1) {
                            if (values[j - 1] > values[j]) {
                                window.alert("Valores de Minimo e Máximo incoerentes.");
                                return;
                            }
                        }
                    }

                    var idSensor = getSelectedSensorID();
                    request("GET", "http://phpdev2.dei.isep.ipp.pt/~arqsi/smartcity/valoresFacetadoSensor.php",
                            "?sensor=" + idSensor + "&faceta=" + child.firstChild.id,
                            "JSON", filterStuff, [values, child.firstChild.id + "", params, i]);
                }
            }
        } else {
            dataTreatment(params, i + 2);
        }
    } else {
        params = params.substring(0, params.length - 1);
        request("GET", "http://phpdev2.dei.isep.ipp.pt/~arqsi/smartcity/valoresDeSensor.php",
                params,
                "JSON", drawResults, values);
    }
}

function filterStuffGPS(response, array) {
    var arr = [response.length * 2];
    for (var i = 0; i < response.length; i++) {
        var splitted = response[i].split(";");
        for (var j = 0; j < splitted.length; j++) {
            splitted[j] = splitted[j].replace(",", ".");
        }
        arr[i] = parseFloat(splitted[0].substring(1, splitted[0].length));
        arr[response.length + i] = parseFloat(splitted[1].substring(1, splitted[1].length));
    }

    var values = array[0];
    var string = array[1] + "=[";
    var params = array[2];

    for (var i = 0; i < arr.length; i++) {
        var valueToCheck = arr[i];
        var valueToCheck2 = arr[i + response.length];
        if (valueToCheck <= values[1] && valueToCheck >= values[0] &&
                valueToCheck2 <= values[3] && valueToCheck2 >= values[2]) {
            string += "N" + valueToCheck + ";W" + valueToCheck2 + ",";
        }
    }

    if (string.includes(",")) {
        string = string.substring(0, string.length - 1) + "]";
    } else {
        window.alert("Nao existem valores para " + array[1] + ". Erro!");
        return;
    }
    params += string + "&";

    dataTreatment(params, array[3] + 2);
}

function filterStuffNumber(response, array) {
    var arr = [response.length];
    for (var i = 0; i < response.length; i++) {
        arr[i] = parseFloat(response[i].replace(",", "."));
    }
    filterStuff(arr, array);
}

function filterStuff(response, array) {
    var values = array[0];
    var string = array[1] + "=[";
    var params = array[2];
    for (var i = 0; i < response.length; i++) {
        var valueToCheck = response[i];
        if (valueToCheck <= values[1] && valueToCheck >= values[0]) {
            string += valueToCheck + ",";
        }
    }

    if (string.includes(",")) {
        string = string.substring(0, string.length - 1) + "]";
    } else {
        window.alert("Nao existem valores para " + array[1] + " especificado. Erro!");
        return;
    }

    params += string + "&";

    dataTreatment(params, array[3] + 2);
}

function drawResults(results, values) {
    var maindiv = document.getElementById("widget_vertical");
    var right_div = document.createElement("div");
    right_div.setAttribute("id", "results");
    right_div.setAttribute("class", "results");
    maindiv.appendChild(right_div);
    var table = document.createElement("table");
    table.setAttribute("border", "dotted");

    //titles
    var titleTR = document.createElement("tr");
    titleTR.setAttribute("align", "center");
    for (var i = 0; i < facetas.childElementCount - 1; i += 2) {
        var th = document.createElement("th");
        th.appendChild(document.createTextNode(facetas.childNodes[i].firstChild.id));
        titleTR.appendChild(th);
    }
    table.appendChild(titleTR);

    //data
    for (var j = 0; j < results.length; j++) {
        var anotherTR = document.createElement("tr");
        anotherTR.setAttribute("align", "center");
        for (var i = 0; i < facetas.childNodes.length - 1; i += 2) {
            var qq = facetas.childNodes[i];
            var obj = results[j];
            var td = document.createElement("td");

            if (qq.firstChild.id == "Foto" && obj[qq.firstChild.id] != "") {
                var img = document.createElement("img");
                if (Math.random() > 0.5) {
                    img.setAttribute("src", "cat1.jpg");
                } else {
                    img.setAttribute("src", "cat2.jpg");
                }
                img.setAttribute("style", "width:60px;height:60px;");
                td.appendChild(img);
            } else {
                td.appendChild(document.createTextNode(obj[qq.firstChild.id]));
            }

            anotherTR.appendChild(td);
        }
        table.appendChild(anotherTR);
    }


    right_div.appendChild(table);
}