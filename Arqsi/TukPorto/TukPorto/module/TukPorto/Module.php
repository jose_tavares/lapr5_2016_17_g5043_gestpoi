<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/TukPorto for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */
namespace TukPorto;

use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use TukPorto\Model\Percurso;
use TukPorto\Model\PontoPercurso;
use TukPorto\Model\PercursoTable;
use TukPorto\Model\TuristaTable;
use TukPorto\Model\PontoPercursoTable;
use TukPorto\Model\Turista;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;

class Module implements AutoloaderProviderInterface
{

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php'
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    // if we're in a namespace deeper than one level we need to fix the \ in the path
                    __NAMESPACE__ => __DIR__ . '/src/' . str_replace('\\', '/', __NAMESPACE__)
                )
            )
        );
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                'TukPorto\Model\PercursoTable' => function ($sm) {
                    $tableGateway = $sm->get('PercursoTableGateway');
                    $table = new Percursotable($tableGateway);
                    return $table;
                },
                'PercursoTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Percurso());
                    return new TableGateway('percurso', $dbAdapter, null, $resultSetPrototype);
                },
                'TukPorto\Model\TuristaTable' => function ($sm) {
                    $tableGateway = $sm->get('TuristaTableGateway');
                    $table = new Turistatable($tableGateway);
                    return $table;
                },
                'TuristaTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Turista());
                    return new TableGateway('turista', $dbAdapter, null, $resultSetPrototype);
                },
                'TukPorto\Model\PontoPercursoTable' => function ($sm) {
                    $tableGateway = $sm->get('PontoPercursoTableGateway');
                    $table = new PontoPercursoTable($tableGateway);
                    return $table;
                },
                'PontoPercursoTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new PontoPercurso());
                    return new TableGateway('pontopercurso', $dbAdapter, null, $resultSetPrototype);
                }
            )
        );
    }

    public function onBootstrap(MvcEvent $e)
    {
        // You may not need to do this if you're doing it elsewhere in your
        // application
        $eventManager = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
    }
}
