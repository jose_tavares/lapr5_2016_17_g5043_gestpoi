<?php
namespace TukPorto\Form;

use Zend\Form\Form;
use TukPorto\Services\WebApiServices;

class PercursoForm extends Form
{

    public function __construct($name = null)
    {
        // we want to ignore the name passed
        parent::__construct('percurso');
        $pois=WebApiServices::getPois();
        $poisName=array();
        foreach($pois as $temp){
            array_push($poisName, $temp['Name']);
        }
        $this->add(array(
            'name' => 'id',
            'type' => 'Hidden'
        ));
        
        $this->add(array(
            'name' => 'nome',
            'type' => 'Text',
            'options' => array(
                'label' => 'Nome:'
            )
        ));
        
        $this->add(array(
            'name' => 'tempoPe',
            'type' => 'Text',
            'options' => array(
                'label' => 'Tempo a pe:'
            )
        ));
        
        $this->add(array(
            'name' => 'tempoCarro',
            'type' => 'Text',
            'options' => array(
                'label' => 'Tempo de carro:'
            )
        ));
        
        
        
        $this->add(array(
            'name' => 'kmTotal',
            'type' => 'Text',
            'options' => array(
                'label' => 'KM Total:'
            )
        ));
        
        $this->add(array(
            'name' => 'tempoPlaneado',
            'type' => 'Text',
            'options' => array(
                'label' => 'Tempo Planeado:'
            )
        ));
        
        $this->add(array(
            'name' => 'tempoViagem',
            'type' => 'Text',
            'options' => array(
                'label' => 'tempoViagem:'
            )
        ));
        

        $this->add(array(
            'type'=>'select',
            'name'=>'pois',
            'options'=>array(
                'label'=>'POIs',
                'multiple'=>'true',
                'value_options'=>$poisName
            )
        ));
        $this->add(array(
            'name' => 'turistaid',
            'type' => 'Hidden'
        ));
        
        $this->add(array(
            'name' => 'submit',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'Go',
                'id' => 'submitbutton'
            )
        ));
    }
}

