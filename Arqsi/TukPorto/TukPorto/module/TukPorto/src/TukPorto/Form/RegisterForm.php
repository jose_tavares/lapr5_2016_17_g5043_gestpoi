<?php
namespace TukPorto\Form;

use Zend\Form\Element;
use Zend\Form\Form;

class RegisterForm extends Form
{
    public function __construct($name = null) {
        // we want to ignore the name passed
        parent::__construct('turista');
        $this->add(array(
            'name' => 'id',
            'type' => 'Hidden',
        ));

        $this->add(array(
            'name' => 'email',
            'type' => 'Email',
            'options' => array(
                'label' => 'Email:' 
            ),
        ));
        $this->add(array(
            'name' => 'password',
            'type' => 'Password',
            'options' => array(
                'label' => 'Password:'
            ),
        ));   
        
        $this->add(array(
            'type'=>'Zend\Form\Element\MultiCheckbox',
            'name'=>'disponibilidade_segunda',
            
            'options'=>array(
                'label'=>'Segunda:',
                'value_options'=>array(
                    '0' => 'Manhã',
                    '1' => 'Tarde',
                    '2' => 'Noite'
                    ),
            )
        ));
        
        $this->add(array(
            'type'=>'Zend\Form\Element\MultiCheckbox',
            'name'=>'disponibilidade_terca',
        
            'options'=>array(
                'label'=>'Terça:',
                'value_options'=>array(
                    '0' => 'Manhã',
                    '1' => 'Tarde',
                    '2' => 'Noite'
                ),
            )
        ));
        
        $this->add(array(
            'type'=>'Zend\Form\Element\MultiCheckbox',
            'name'=>'disponibilidade_quarta',
        
            'options'=>array(
                'label'=>'Quarta:',
                'value_options'=>array(
                    '0' => 'Manhã',
                    '1' => 'Tarde',
                    '2' => 'Noite'
                ),
            )
        ));
        
        $this->add(array(
            'type'=>'Zend\Form\Element\MultiCheckbox',
            'name'=>'disponibilidade_quinta',
        
            'options'=>array(
                'label'=>'Quinta:',
                'value_options'=>array(
                    '0' => 'Manhã',
                    '1' => 'Tarde',
                    '2' => 'Noite'
                ),
            )
        ));
        
        $this->add(array(
            'type'=>'Zend\Form\Element\MultiCheckbox',
            'name'=>'disponibilidade_sexta',
        
            'options'=>array(
                'label'=>'Sexta:',
                'value_options'=>array(
                    '0' => 'Manhã',
                    '1' => 'Tarde',
                    '2' => 'Noite'
                ),
            )
        ));
        
        $this->add(array(
            'type'=>'Zend\Form\Element\MultiCheckbox',
            'name'=>'disponibilidade_sabado',
        
            'options'=>array(
                'label'=>'Sábado:',
                'value_options'=>array(
                    '0' => 'Manhã',
                    '1' => 'Tarde',
                    '2' => 'Noite'
                ),
            )
        ));
        
        $this->add(array(
            'type'=>'Zend\Form\Element\MultiCheckbox',
            'name'=>'disponibilidade_domingo',
        
            'options'=>array(
                'label'=>'Domingo:',
                'value_options'=>array(
                    '0' => 'Manhã',
                    '1' => 'Tarde',
                    '2' => 'Noite'
                ),
            )
        ));
        
        $this->add(array(
            'type'=>'Zend\Form\Element\Checkbox',
            'name'=>'acessibilidade',
            'options' => array(
                'label' => 'Acessibilidade:',
                'use_hidden_element' => true,
                'checked_value' => 'True',
                'unchecked_value' => 'False'
            )
        ));
        
        $this->add(array(
            'name' => 'submit',
            'type' => 'button',
            'attributes' => array(
                'value' => 'Go',
                'id' => 'submitbutton',
            ),
        ));
    }
}

