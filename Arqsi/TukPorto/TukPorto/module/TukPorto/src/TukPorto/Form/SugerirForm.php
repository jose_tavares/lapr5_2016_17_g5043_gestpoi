<?php
namespace TukPorto\Form;

use Zend\Form\Element;
use Zend\Form\Form;

class SugerirForm extends Form
{
    public function __construct($name=null){
        parent::__construct('pontoPercurso');
        $this->add(array(
            'name' => 'id',
            'type' => 'Hidden',
        ));
        
        $this->add(array(
            'name' => 'nome',
            'type' => 'Text',
            'options' => array(
                'label' => 'Nome:'
            ),
        ));
        
        $this->add(array(
            'name' => 'descricao',
            'type' => 'Text',
            'options' => array(
                'label' => 'Descricao:'
            ),
        ));
        
        $this->add(array(
            'name' => 'local',
            'type' => 'Text',
            'options' => array(
                'label' => 'Local:'
            ),
        ));
        
        $this->add(array(
            'name' => 'gps_lat',
            'type' => 'Text',
            'options' => array(
                'label' => 'Latitude:'
            ),
        ));
        
        $this->add(array(
            'name' => 'gps_long',
            'type' => 'Text',
            'options' => array(
                'label' => 'Longitude:'
            ),
        ));
        
        $this->add(array(
            'name' => 'hashtags',
            'type' => 'Text',
            'options' => array(
                'label' => 'Hashtags:'
            ),
        ));
        
        $this->add(array(
            'name' => 'tempoVisita',
            'type' => 'Text',
            'options' => array(
                'label' => 'Tempo Visita:'
            ),
        ));
        
        $this->add(array(
            'type'=>'Zend\Form\Element\MultiCheckbox',
            'name'=>'disponibilidade_segunda',
        
            'options'=>array(
                'label'=>'Segunda:',
                'value_options'=>array(
                    '0' => 'Manha',
                    '1' => 'Tarde',
                    '2' => 'Noite'
                ),
            )
        ));
        
        $this->add(array(
            'type'=>'Zend\Form\Element\MultiCheckbox',
            'name'=>'disponibilidade_terca',
        
            'options'=>array(
                'label'=>'Terca:',
                'value_options'=>array(
                    '0' => 'Manha',
                    '1' => 'Tarde',
                    '2' => 'Noite'
                ),
            )
        ));
        
        $this->add(array(
            'type'=>'Zend\Form\Element\MultiCheckbox',
            'name'=>'disponibilidade_quarta',
        
            'options'=>array(
                'label'=>'Quarta:',
                'value_options'=>array(
                    '0' => 'Manha',
                    '1' => 'Tarde',
                    '2' => 'Noite'
                ),
            )
        ));
        
        $this->add(array(
            'type'=>'Zend\Form\Element\MultiCheckbox',
            'name'=>'disponibilidade_quinta',
        
            'options'=>array(
                'label'=>'Quinta:',
                'value_options'=>array(
                    '0' => 'Manha',
                    '1' => 'Tarde',
                    '2' => 'Noite'
                ),
            )
        ));
        
        $this->add(array(
            'type'=>'Zend\Form\Element\MultiCheckbox',
            'name'=>'disponibilidade_sexta',
        
            'options'=>array(
                'label'=>'Sexta:',
                'value_options'=>array(
                    '0' => 'Manha',
                    '1' => 'Tarde',
                    '2' => 'Noite'
                ),
            )
        ));
        
        $this->add(array(
            'type'=>'Zend\Form\Element\MultiCheckbox',
            'name'=>'disponibilidade_sabado',
        
            'options'=>array(
                'label'=>'Sabado:',
                'value_options'=>array(
                    '0' => 'Manha',
                    '1' => 'Tarde',
                    '2' => 'Noite'
                ),
            )
        ));
        
        $this->add(array(
            'type'=>'Zend\Form\Element\MultiCheckbox',
            'name'=>'disponibilidade_domingo',
        
            'options'=>array(
                'label'=>'Domingo:',
                'value_options'=>array(
                    '0' => 'Manha',
                    '1' => 'Tarde',
                    '2' => 'Noite'
                ),
            )
        ));
        
        $this->add(array(
            'type'=>'Zend\Form\Element\Checkbox',
            'name'=>'acessibilidade',
            'options' => array(
                'label' => 'Acessibilidade:',
                'use_hidden_element' => true,
                'checked_value' => 'True',
                'unchecked_value' => 'False'
            )
        ));
        
        $this->add(array(
            'name' => 'submit',
            'type' => 'Button',
            'attributes' => array(
                'value' => 'Go',
                'id' => 'submitbutton',
            ),
        ));
    }
}