<?php
namespace TukPorto\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use TukPorto\Services\WebApiServices;
use Zend\Json\Json;
use TukPorto\Model\PontoPercurso;
use TukPorto\Form\SugerirForm;
use TukPorto\Form\EditPoiForm;
use TukPorto\Model\Disponibilidade;
use TukPorto\Model\Hashtag;

class PontoPercursoController extends AbstractActionController
{

    protected $pontopercursoTable;

    protected $percursoTable;

    public function indexAction()
    {
        $id = (int) $this->params()->fromRoute('id', 0);
        return new ViewModel(array(
            'pontospercurso' => WebApiServices::getPois(),
            'id' => $id
        ));
    }
    
    public function editAction(){
        $form=new EditPoiForm();
        $request = $this->getRequest();
        return array('form'=>$form);
    }
    
    public function detailsAction(){
        $id = (int) $this->params()->fromRoute('id', 0);
        return new ViewModel(array(
            'pontopercurso' => WebApiServices::getPoi($id),
            'id' => $id
        ));
    }
    
    public function sugButton($form){
        $poi=new PontoPercurso();
        $hashtags=array();
        $disponibilidades=array();
        $poi->id=1;
        $poi->nome=$form->get('nome')->getValue();
        $poi->descricao=$form->get('descricao')->getValue();
        $poi->local=$form->get('local')->getValue();
        $poi->gps_lat=$form->get('gps_lat')->getValue();
        $poi->gps_long=$form->get('gps_long')->getValue();
        $poi->tempoVisita=$form->get('tempoVisita')->getValue();
        $poi->validado=false;
        $temp=$form->get('disponibilidade_segunda')->getValue();
        foreach($temp as $disp){
            $disptemp=new Disponibilidade();
            $disptemp->DisponibilidadeID=1;
            $disptemp->Day=2;
            $disptemp->TimeOfDay=$disp;
            array_push($disponibilidades,$disptemp);
        }
        $temp=$form->get('disponibilidade_terca')->getValue();
        foreach($temp as $disp){
            $disptemp=new Disponibilidade();
            $disptemp->DisponibilidadeID=1;
            $disptemp->Day=3;
            $disptemp->TimeOfDay=$disp;
            array_push($disponibilidades,$disptemp);
        }
        $temp=$form->get('disponibilidade_quarta')->getValue();
        foreach($temp as $disp){
            $disptemp=new Disponibilidade();
            $disptemp->DisponibilidadeID=1;
            $disptemp->Day=4;
            $disptemp->TimeOfDay=$disp;
            array_push($disponibilidades,$disptemp);
        }
        $temp=$form->get('disponibilidade_quinta')->getValue();
        foreach($temp as $disp){
            $disptemp=new Disponibilidade();
            $disptemp->DisponibilidadeID=1;
            $disptemp->Day=5;
            $disptemp->TimeOfDay=$disp;
            array_push($disponibilidades,$disptemp);
        }
        $temp=$form->get('disponibilidade_sexta')->getValue();
        foreach($temp as $disp){
            $disptemp=new Disponibilidade();
            $disptemp->DisponibilidadeID=1;
            $disptemp->Day=6;
            $disptemp->TimeOfDay=$disp;
            array_push($disponibilidades,$disptemp);
        }
        $temp=$form->get('disponibilidade_sabado')->getValue();
        foreach($temp as $disp){
            $disptemp=new Disponibilidade();
            $disptemp->DisponibilidadeID=1;
            $disptemp->Day=7;
            $disptemp->TimeOfDay=$disp;
            array_push($disponibilidades,$disptemp);
        }
        $temp=$form->get('disponibilidade_domingo')->getValue();
        foreach($temp as $disp){
            $disptemp=new Disponibilidade();
            $disptemp->DisponibilidadeID=1;
            $disptemp->Day=8;
            $disptemp->TimeOfDay=$disp;
            array_push($disponibilidades,$disptemp);
        }
        $poi->disponibilidade=$disponibilidades;
        $poi->acessibilidade=$form->get('acessibilidade')->getValue();
        $hashtemp=$form->get('hashtags')->getValue();
        $hashsplit=preg_split("/,/",$hashtemp);
        foreach($hashsplit as $hash){
            $hashtag=new Hashtag();
            $hashtag->HashtagID=1;
            $hashtag->Name=$hash;
            array_push($hashtags,$hashtag);
        }
        $sugestao=array('LocalID'=>$poi->local,'POIID'=>$poi->id,'Name'=>$poi->nome,'Description'=>$poi->descricao,'hashtags'=>$poi->hashtags,'tempoVisita'=>$poi->tempoVisita,'dias'=>$poi->disponibilidade,'validado'=>$poi->validado);
        WebApiServices::sugerirPoi($sugestao);
        return $this->redirect()->toRoute('pontopercurso');
    }
    
    public function sugerirAction(){
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
        if (! isset($_SESSION['turista'])) {
            return $this->redirect()->toRoute('turista');
        }
        $form = new SugerirForm();
        $form->get('submit')->setValue('Registar');
        $request = $this->getRequest();
       $form->get('submit')->setAttribute('onclick', 'PontoPercursoController::sugButton($form)');
        return array('form'=>$form);
    }

    

    public function poisAction()
    {
        $id = (int) $this->params()->fromRoute('id', 0);
        if (! $id) {
            return $this->redirect()->toRoute('percurso', array(
                'action' => 'index'
            ));
        }
        
        // Check if turista is owner of percurso
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
        
        $percurso = $this->getPercursoTable()->getPercurso($id);
        
        if ($_SESSION['turistaid'] == null || $_SESSION['turistaid'] != $percurso->turistaid) {
            return $this->redirect()->toRoute('turista');
        }
        
        $pois = WebApiServices::getPois();
        
        return new ViewModel(array(
            'pois' => $pois,
            'id' => $id
        ));
    }

    public function meteorologiasAction()
    {
        $id = (int) $this->params()->fromRoute('id', 0);
        if (! $id) {
            return $this->redirect()->toRoute('percurso', array(
                'action' => 'index'
            ));
        }
        
        $ponto = $this->getPontoPercursoTable()->getPontoPercurso($id);
        
        $json = WebApiServices::getMeteoDatabyPoiId($ponto->idpoi);
        $meteorologias = Json::decode($json);
        
        return array(
            'ponto' => $ponto,
            'meteorologias' => $meteorologias
        );
    }

    public function addAction()
    {
        $idb = (string) $this->params()->fromRoute('id', 0);
        if (! $idb) {
            return $this->redirect()->toRoute('pois', array(
                'action' => 'index',
                'id' => $idb
            ));
        }
        
        $pieces = explode("_", $idb);
        $idpoi =  $pieces[0]; 
        $idpercurso  = $pieces[1]; 
        
        // Check if turista is owner of percurso
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
        $percurso = $this->getPercursoTable()->getPercurso($idpercurso);
        if ($_SESSION['turistaid'] == null || $_SESSION['turistaid'] != $percurso->turistaid) {
            return $this->redirect()->toRoute('turista');
        }
        
        $poi = WebApiServices::getPoi($idpoi);
        $ponto = new PontoPercurso();
        
        $ponto->idpoi = $idpoi;
        $ponto->idpercurso = $idpercurso;
        $ponto->nome = $poi['Name'];
        $ponto->descricao = $poi['Description'];
        $ponto->local = $poi['Local']['Name'];
        $ponto->gps_lat = $poi['Local']['GPS_Lat'];
        $ponto->gps_long = $poi['Local']['GPS_Long'];
        
        $this->getPontoPercursoTable()->savePontoPercurso($ponto);
        // Fazer redirect melhor
        return $this->redirect()->toRoute('pontopercurso', array('id' => $idpercurso));
        
    }

    public function poiAction()
    {
        $id = (int) $this->params()->fromRoute('id', 0);
        if (! $id) {
            return $this->redirect()->toRoute('percurso', array(
                'action' => 'index'
            ));
        }
        
        $json = WebApiServices::getMeteoDatabyPoiId($id);
        $meteorologias = Json::decode($json);
        
        $jsonPoi = WebApiServices::getPoi($id);
        
        return array(
            'poi' => $jsonPoi,
            'meteorologias' => $meteorologias
        );
    }

    public function deleteAction()
    {
        $id = (int) $this->params()->fromRoute('id', 0);
        if (! $id) {
            return $this->redirect()->toRoute('pontopercurso');
        }
        $request = $this->getRequest();
        
        $pontopercurso = $this->getPontoPercursoTable()->getPontoPercurso($id);
        // Check if turista is owner of percurso
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
        
        $percurso = $this->getPercursoTable()->getPercurso($pontopercurso->idpercurso);
        
        if ($_SESSION['turistaid'] == null || $_SESSION['turistaid'] != $percurso->turistaid) {
            return $this->redirect()->toRoute('turista');
        }
        
        if ($request->isPost()) {
            $del = $request->getPost('del', 'No');
            if ($del == 'Sim') {
                $id = (int) $request->getPost('id');
                $this->getPontoPercursoTable()->deletePontoPercurso($id);
            }
            // Redirect to list of percursos
            return $this->redirect()->toRoute('pontopercurso', array(
                'id' => $percurso->id
            ));
        }
        return array(
            'id' => $id,
            'pontopercurso' => $this->getPontoPercursoTable()->getPontoPercurso($id)
        );
    }

    public function getPontoPercursoTable()
    {
        if (! $this->pontopercursoTable) {
            $sm = $this->getServiceLocator();
            $this->pontopercursoTable = $sm->get('TukPorto\Model\PontoPercursoTable');
        }
        
        return $this->pontopercursoTable;
    }

    public function getPercursoTable()
    {
        if (! $this->percursoTable) {
            $sm = $this->getServiceLocator();
            $this->percursoTable = $sm->get('TukPorto\Model\PercursoTable');
        }
        
        return $this->percursoTable;
    }
}

