<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/TukPorto for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */
namespace TukPorto\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use TukPorto\Form\PercursoForm;
use TukPorto\Model\Percurso;
use TukPorto\Services\WebApiServices;

class PercursoController extends AbstractActionController
{

    protected $percursoTable;

    public function indexAction()
    {
        $percursos = array(
            'percursos' => WebApiServices::getPercursos()
        );
        
        return new ViewModel($percursos);
    }

    public function addAction()
    {
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
        if (! isset($_SESSION['turista'])) {
            return $this->redirect()->toRoute('turista');
        }
        
        $form = new PercursoForm();
        $form->get('submit')->setValue('Add');
        
        $request = $this->getRequest();
        if ($request->isPost()) {
            $percurso = new Percurso();
            $form->setInputFilter($percurso->getInputFilter());
            $form->setData($request->getPost());
            
            if ($form->isValid()) {
                $percurso->exchangeArray($form->getData());
                
                // Redirect to list of percursos
                return $this->redirect()->toRoute('percurso');
            }
        }
        return array(
            'form' => $form
        );
    }

    public function editAction()
    {
        $id = (int) $this->params()->fromRoute('id', 0);
        if (! $id) {
            return $this->redirect()->toRoute('percurso', array(
                'action' => 'index'
            ));
        }
        // Get the Percurso with the specified id. An exception is thrown
        // if it cannot be found, in which case go to the index page.
        try {
            $percurso = $this->getPercursoTable()->getPercurso($id);
        } catch (\Exception $ex) {
            return $this->redirect()->toRoute('percurso', array(
                'action' => 'index'
            ));
        }
        
        // Check if turista is owner of percurso
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
        if ($_SESSION['turistaid'] == null || $_SESSION['turistaid'] != $percurso->turistaid) {
            return $this->redirect()->toRoute('turista');
        }
        
        $form = new PercursoForm();
        $form->bind($percurso);
        $form->get('submit')->setAttribute('value', 'Edit');
        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->setInputFilter($percurso->getInputFilter());
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $this->getPercursoTable()->savePercurso($percurso);
                // Redirect to list of percursos
                return $this->redirect()->toRoute('percurso');
            }
        }
        return array(
            'id' => $id,
            'form' => $form
        );
    }

    public function deleteAction()
    {
        
        $id = (int) $this->params()->fromRoute('id', 0);
        if (! $id) {
            return $this->redirect()->toRoute('percurso');
        }
        $request = $this->getRequest();
        
        $percurso = $this->getPercursoTable()->getPercurso($id);
        // Check if turista is owner of percurso
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
        if ($_SESSION['turistaid'] == null || $_SESSION['turistaid'] != $percurso->turistaid) {
            return $this->redirect()->toRoute('turista');
        }
        
        if ($request->isPost()) {
            //return $this->redirect()->toRoute('meow');
            $del = $request->getPost('del', 'No');
            if ($del == 'Sim') {
                $id = (int) $request->getPost('id');
                $this->getPercursoTable()->deletePercurso($id);
            }
            // Redirect to list of percursos
            return $this->redirect()->toRoute('percurso');
        }
        return array(
            'id' => $id,
            'percurso' => $this->getPercursoTable()->getPercurso($id)
        );
    }

    public function getPercursoTable()
    {
        if (! $this->percursoTable) {
            $sm = $this->getServiceLocator();
            $this->percursoTable = $sm->get('TukPorto\Model\PercursoTable');
        }
        
        return $this->percursoTable;
    }
}
