<?php
namespace TukPorto\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use TukPorto\Form\LoginForm;
use TukPorto\Form\RegisterForm;
use TukPorto\Model\Turista;
use Zend\View\Model\ViewModel;
use TukPorto\Services\WebApiServices;
use TukPorto\Model\Disponibilidade;

class TuristaController extends AbstractActionController
{

    protected $turistaTable;

    public function loginAction()
    {
        $request = $this->getRequest();
        if (! $request->isPost()) {
            $form = new LoginForm();
            $form->get('submit')->setValue('Login');
            return array(
                'form' => $form
            );
        } else {
            $name = $request->getPost('email');
            $pass = $request->getPost('password');
            
            $loginResult=WebApiServices::DBLogin($name,$pass);
            
            $idTurista=$loginResult['ID'];
            
            if ($idTurista > 0) {
                if (session_status() == PHP_SESSION_NONE) {
                    session_start();
                }
                $_SESSION['turista'] = $name;
                $_SESSION['turistaid'] = $idTurista;
                return $this->redirect()->toRoute('home');
            }
            
            return $this->redirect()->toRoute('turista');
        }
    }

    public function logoutAction()
    {
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
        $_SESSION['turista'] = null;
        $_SESSION['turistaid'] = null;
        return $this->redirect()->toRoute('home');
    }

    public function infoAction()
    {
        return new ViewModel(array(
            'turista' => $this->getTuristaTable()->getTuristaBySession()
        ));
    }

    public function registerAction()
    {
        $form = new RegisterForm();
        $form->get('submit')->setValue('Registar');
        $request = $this->getRequest();
        if ($request->isPost()) {
            $turista = new Turista();
            $form->setInputFilter($turista->getInputFilter());
            $form->setData($request->getPost());
            
            if ($form->isValid()) {
                $turista->email=$form->get('email')->getValue();
                $turista->password=$form->get('password')->getValue();
                $disponibilidades=array();
                $temp=$form->get('disponibilidade_segunda')->getValue();
                foreach($temp as $disp){
                    $disptemp=new Disponibilidade();
                    $disptemp->DisponibilidadeID=1;
                    $disptemp->Day=2;
                    $disptemp->TimeOfDay=$disp;
                    array_push($disponibilidades,$disptemp);
                }
                $temp=$form->get('disponibilidade_terca')->getValue();
                foreach($temp as $disp){
                    $disptemp=new Disponibilidade();
                    $disptemp->DisponibilidadeID=1;
                    $disptemp->Day=3;
                    $disptemp->TimeOfDay=$disp;
                    array_push($disponibilidades,$disptemp);
                }
                $temp=$form->get('disponibilidade_quarta')->getValue();
                foreach($temp as $disp){
                    $disptemp=new Disponibilidade();
                    $disptemp->DisponibilidadeID=1;
                    $disptemp->Day=4;
                    $disptemp->TimeOfDay=$disp;
                    array_push($disponibilidades,$disptemp);
                }
                $temp=$form->get('disponibilidade_quinta')->getValue();
                foreach($temp as $disp){
                    $disptemp=new Disponibilidade();
                    $disptemp->DisponibilidadeID=1;
                    $disptemp->Day=5;
                    $disptemp->TimeOfDay=$disp;
                    array_push($disponibilidades,$disptemp);
                }
                $temp=$form->get('disponibilidade_sexta')->getValue();
                foreach($temp as $disp){
                    $disptemp=new Disponibilidade();
                    $disptemp->DisponibilidadeID=1;
                    $disptemp->Day=6;
                    $disptemp->TimeOfDay=$disp;
                    array_push($disponibilidades,$disptemp);
                }
                $temp=$form->get('disponibilidade_sabado')->getValue();
                foreach($temp as $disp){
                    $disptemp=new Disponibilidade();
                    $disptemp->DisponibilidadeID=1;
                    $disptemp->Day=7;
                    $disptemp->TimeOfDay=$disp;
                    array_push($disponibilidades,$disptemp);
                }
                $temp=$form->get('disponibilidade_domingo')->getValue();
                foreach($temp as $disp){
                    $disptemp=new Disponibilidade();
                    $disptemp->DisponibilidadeID=1;
                    $disptemp->Day=8;
                    $disptemp->TimeOfDay=$disp;
                    array_push($disponibilidades,$disptemp);
                }              
                $turista->disponibilidade=$disponibilidades;
                $turista->acessibilidade=$form->get('acessibilidade')->getValue();
                $register=array('Email'=>$turista->email,'Password'=>$turista->password,'ConfirmPassword'=>$turista->password,'disp'=>$turista->disponibilidade,'acessivel'=>$turista->acessibilidade);
                WebApiServices::registerUser($register);
                $_SESSION['turista'] = null;
                $_SESSION['turistaid'] = null;
                return $this->redirect()->toRoute('home');
            }
        }
        
        return array(
            'form' => $form
        );
    }

    public function getTuristaTable()
    {
        if (! $this->turistaTable) {
            $sm = $this->getServiceLocator();
            $this->turistaTable = $sm->get('TukPorto\Model\TuristaTable');
        }
        return $this->turistaTable;
    }
}

