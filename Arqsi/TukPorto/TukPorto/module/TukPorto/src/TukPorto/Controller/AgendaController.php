<?php
namespace TukPorto\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use TukPorto\Form\AgendaForm;
use TukPorto\Model\Agenda;
use TukPorto\Services\WebApiServices;

class AgendaController extends AbstractActionController
{
    public function indexAction(){
        $agendas = array(
            'agendas' => WebApiServices::getAgendas()
        );
        
        return new ViewModel($agendas);
        
    }
}