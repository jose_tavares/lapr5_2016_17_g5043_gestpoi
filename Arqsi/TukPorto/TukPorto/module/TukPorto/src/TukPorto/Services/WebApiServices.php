<?php
namespace TukPorto\Services;

use Zend\Http\Client;
use Zend\Http\Request;
use Zend\Json\Json;

class WebApiServices
{

    public static $username = "tukporto@tuk.pt";

    public static $password = "TukPorto1!";

    public static $usernameT = "tukporto@tuk.pt";
    
    public static $passwordT = "TukPorto1!";
    
    public static $enderecoBase = 'https://10.8.11.93:444/CancelaW';

    public static function Login()
    {
        $username = WebApiServices::$username;
        $password = WebApiServices::$password;
        $enderecoBase = WebApiServices::$enderecoBase;
        $client = new Client($enderecoBase . '/Token');
        $client->setMethod(Request::METHOD_POST);
        $data = "grant_type=password&username=$username&password=$password";
        $len = strlen($data);
        $client->setHeaders(array(
            'Content-Type' => 'application/x-www-form-urlencoded',
            'Content-Length' => $len
        ));
        $client->setOptions([
                      'adapter' => '\Zend\Http\Client\Adapter\Curl',
            'curloptions' => array(CURLOPT_SSL_VERIFYPEER => FALSE,
                CURLOPT_SSL_VERIFYHOST => FALSE),
        ]);
        $client->setRawBody($data);
        $response = $client->send();
        $body = Json::decode($response->getBody());
        if (! empty($body->access_token)) {
            if (! isset($_SESSION)) {
                session_start();
            }
            $_SESSION['access_token'] = $body->access_token;
            $_SESSION['username'] = $username;
            return true;
        } else
            return false;
    }

    public static function Logout()
    {
        if (! isset($_SESSION)) {
            session_start();
        }
        $_SESSION['username'] = null;
        $_SESSION['access_token'] = null;
    }

    public static function DBLogin($username, $password){
        WebApiServices::$username = $username;
        WebApiServices::$password = $password;
        if (! isset($_SESSION)) {
            session_start();
        }
        if (! isset($_SESSION['access_token'])) {
            WebApiServices::Login();
        }
    
        $client = new Client(WebApiServices::$enderecoBase . '/api/Account/UserInfo');
        $client->setMethod(Request::METHOD_GET);
        $bearer_token = 'Bearer ' . $_SESSION['access_token'];
        $client->setHeaders(array(
            'Authorization' => $bearer_token
        ));
        $client->setOptions([
            'adapter' => '\Zend\Http\Client\Adapter\Curl',
            'curloptions' => array(CURLOPT_SSL_VERIFYPEER => FALSE,
                CURLOPT_SSL_VERIFYHOST => FALSE),
            /*'sslverifypeer' => false,
             'verify_peer' => false,
        'allow_self_signed' => true,
        'ssl_verify_host' => false */
        ]);
        $response = $client->send();
        $body = $response->getBody();
        $userinfo = Json::decode($response->getBody(), true);
        return $userinfo;
    }
    
public static function getPois()
    {
        if (! isset($_SESSION)) {
            session_start();
        }
        if (! isset($_SESSION['access_token'])) {
            WebApiServices::Login();
        }
        
        $client = new Client(WebApiServices::$enderecoBase . '/api/POIs');
        $client->setMethod(Request::METHOD_GET);
        $bearer_token = 'Bearer ' . $_SESSION['access_token'];
        $client->setHeaders(array(
            'Authorization' => $bearer_token
        ));
        $client->setOptions([
            'adapter' => '\Zend\Http\Client\Adapter\Curl',
            'curloptions' => array(CURLOPT_SSL_VERIFYPEER => FALSE,
            CURLOPT_SSL_VERIFYHOST => FALSE),
        ]);
        $response = $client->send();
        $body = $response->getBody();
        $pois = Json::decode($response->getBody(), true);
        return $pois;
    }
    

    public static function getPoi($id){
        if (! isset($_SESSION)) {
            session_start();
        }
        if (! isset($_SESSION['access_token'])) {
            WebApiServices::Login();
        }
        
        $client = new Client(WebApiServices::$enderecoBase . '/api/POIs/' . $id);
        $client->setMethod(Request::METHOD_GET);
        $bearer_token = 'Bearer ' . $_SESSION['access_token'];
        $client->setHeaders(array(
            'Authorization' => $bearer_token
        ));
        $client->setOptions([
            'adapter' => '\Zend\Http\Client\Adapter\Curl',
            'curloptions' => array(CURLOPT_SSL_VERIFYPEER => FALSE,
             CURLOPT_SSL_VERIFYHOST => FALSE),
        ]);
        $response = $client->send();
        $body = $response->getBody();
        $poi = Json::decode($response->getBody(), true);
        return $poi;
    }

    public static function getPercursos(){
        if (! isset($_SESSION)) {
            session_start();
        }
        if (! isset($_SESSION['access_token'])) {
            WebApiServices::Login();
        }
        
        $client = new Client(WebApiServices::$enderecoBase . '/api/Percursoes');
        $client->setMethod(Request::METHOD_GET);
        $bearer_token = 'Bearer ' . $_SESSION['access_token'];
        $client->setHeaders(array(
            'Authorization' => $bearer_token
        ));
        $client->setOptions([
            'adapter' => '\Zend\Http\Client\Adapter\Curl',
            'curloptions' => array(CURLOPT_SSL_VERIFYPEER => FALSE,
                CURLOPT_SSL_VERIFYHOST => FALSE),
        ]);
        $response = $client->send();
        $body = $response->getBody();
        $percursos = Json::decode($response->getBody(), true);
        return $percursos;
    }
    
    public static function getAgendas(){
        if (! isset($_SESSION)) {
            session_start();
        }
        if (! isset($_SESSION['access_token'])) {
            WebApiServices::Login();
        }
    
        $client = new Client(WebApiServices::$enderecoBase . '/api/Agenda');
        $client->setMethod(Request::METHOD_GET);
        $bearer_token = 'Bearer ' . $_SESSION['access_token'];
        $client->setHeaders(array(
            'Authorization' => $bearer_token
        ));
        $client->setOptions([
            'adapter' => '\Zend\Http\Client\Adapter\Curl',
            'curloptions' => array(CURLOPT_SSL_VERIFYPEER => FALSE,
                CURLOPT_SSL_VERIFYHOST => FALSE),
        ]);
        $response = $client->send();
        $body = $response->getBody();
        $agendas = Json::decode($response->getBody(), true);
        return $agendas;
    }
    
    public static function getMeteoDatabyPoiId($poiid)
    {
        if (! isset($_SESSION)) {
            session_start();
        }
        if (! isset($_SESSION['access_token'])) {
            WebApiServices::Login();
        }
        
        $date = str_replace('-', '/', date("Y-m-d"));
        $diaY = date('Y-m-d', strtotime($date . "-1 days"));
        $diaYY = date('Y-m-d', strtotime($date . "-2 days"));
        
        // $var = $_SESSION;
        $client = new Client(WebApiServices::$enderecoBase . '/api/Meteorologies/' . $poiid . '?&dataMin=' . $diaYY . '&dataMax=' . $diaY);
        $client->setMethod(Request::METHOD_GET);
        $bearer_token = 'Bearer ' . $_SESSION['access_token'];
        
        $client->setHeaders(array(
            'Authorization' => $bearer_token
        ));
        $client->setOptions([
            'sslverifypeer' => false
        ]);
        
        $response = $client->send();
        $body = $response->getBody();
        return $body;
    }
    
    public static function registerUser($turista){
        if (! isset($_SESSION)) {
            session_start();
        }
        if (! isset($_SESSION['access_token'])) {
            WebApiServices::Login();
        }
        
       $json=json_encode($turista);
       $len=strlen($json);
        $client = new Client(WebApiServices::$enderecoBase . 'api/Account/Register');
        $client->setMethod(Request::METHOD_POST);
        $client->setHeaders(array(
            'Content-Type' => 'application/x-www-form-urlencoded',
            'Content-Length' => $len
        ));
        $client->setOptions([
            'adapter' => '\Zend\Http\Client\Adapter\Curl',
            'curloptions' => array(CURLOPT_SSL_VERIFYPEER => FALSE,
                CURLOPT_SSL_VERIFYHOST => FALSE),
        ]);
        $client->setRawBody($json);
        
        $response=$client->send();
        $bearer_token = 'Bearer ' . $_SESSION['access_token'];
        
    }
    
    public static function sugerirPoi($poi){
        $json=json_encode($poi);
        $len=strlen($json);
        $client = new Client(WebApiServices::$enderecoBase . 'api/POIs');
        $client->setMethod(Request::METHOD_POST);
        $client->setHeaders(array(
            'Content-Type' => 'application/x-www-form-urlencoded',
            'Content-Length' => $len
        ));
        $client->setOptions([
            'adapter' => '\Zend\Http\Client\Adapter\Curl',
            'curloptions' => array(CURLOPT_SSL_VERIFYPEER => FALSE,
                CURLOPT_SSL_VERIFYHOST => FALSE),
        ]);
        $client->setRawBody($json);
        $response=$client->send();
        $bearer_token = 'Bearer ' . $_SESSION['access_token'];
    }
    
    
}

