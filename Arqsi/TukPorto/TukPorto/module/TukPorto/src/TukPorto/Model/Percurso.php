<?php
namespace TukPorto\Model;

use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class Percurso implements InputFilterAwareInterface
{

    public $id;
    
    public $turistaid;
    
    public $listaPoi;

    public $nome;
    
    public $tempoPe;
    
    public $tempoCarro;
    
    public $kmTotal;
    
    public $tempoPlaneado;
    
    public $tempoViagem;
    
    public $predefinido;

    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->id = (! empty($data['id'])) ? $data['id'] : null;
        $this->turistaid = (! empty($data['turistaid'])) ? $data['turistaid'] : null;
        $this->listaPoi=(! empty($data['listaPoi'])) ? $data['listaPoi'] : null;
        $this->nome=(! empty($data['nome'])) ? $data['nome'] : null;
        $this->tempoPe=(! empty($data['tempoPe'])) ? $data['tempoPe'] : null;
        $this->tempoCarro=(! empty($data['tempoCarro'])) ? $data['tempoCarro'] : null;
        $this->kmTotal=(! empty($data['kmTotal'])) ? $data['kmTotal'] : null;
        $this->tempoPlaneado=(! empty($data['tempoPlaneado'])) ? $data['tempoPlaneado'] : null;
        $this->tempoViagem=(! empty($data['tempoViagem'])) ? $data['tempoPlaneado'] : null;
        $this->predefinido=(! empty($data['predefinido'])) ? $data['predefinido'] : null;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        if (! $this->inputFilter) {
            $inputFilter = new InputFilter();
            $inputFilter->add(array(
                'name' => 'id',
                'required' => true,
                'filters' => array(
                    array(
                        'name' => 'Int'
                    )
                )
            ));
            
            $inputFilter->add(array(
                'name' => 'descricao',
                'required' => true,
                'filters' => array(
                    array(
                        'name' => 'StripTags'
                    ),
                    array(
                        'name' => 'StringTrim'
                    )
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min' => 6,
                            'max' => 1000
                        )
                    )
                )
            ));
            $this->inputFilter = $inputFilter;
        }
        return $this->inputFilter;
    }
}

