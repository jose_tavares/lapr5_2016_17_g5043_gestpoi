<?php
namespace TukPorto\Model;

use Zend\Db\TableGateway\TableGateway;

class TuristaTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function getTurista($id)
    {
        $id = (int) $id;
        $rowSet = $this->tableGateway->select(array(
            'id' => $id
        ));
        $row = $rowSet->current();
        if (! $row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    public function getTuristaIdByEmail($email, $pass)
    {
        $rowset = $this->tableGateway->select(array(
            'email' => $email,
            'password' => $pass
        ));
        $row = $rowset->current();

        if (empty($row)) {
            return null;
        }
        return $row->id;  
    }

    public function getTuristaBySession()
    {
        session_start();
        $rowset = $this->tableGateway->select(array(
            'email' => $_SESSION['turista']
        ));
        $row = $rowset->current();
    
        if (empty($row)) {
            return null;
        }
        return $row;
    }
    
    
    public function saveTurista(Turista $turista)
    {
        $data = array(
            'nome' => $turista->nome,
            'nacionalidade' => $turista->nacionalidade,
            'email' => $turista->email,
            'password' => $turista->password,
            'disponibilidade'=>$turista->disponibilidade,
            'acessibilidade'=>$turista->acessibilidade
        );
        
        $id = (int) $turista->id;
        if ($id == 0) {
            $this->tableGateway->insert($data);
        } else {
            if ($this->getTurista($id)) {
                $this->tableGateway->update($data, array(
                    'id' => $id
                ));
            } else {
                throw new \Exception("Turista id does not exist");
            }
        }
    }
}

