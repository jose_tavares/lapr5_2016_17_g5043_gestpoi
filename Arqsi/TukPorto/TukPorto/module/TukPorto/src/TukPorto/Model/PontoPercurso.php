<?php
namespace TukPorto\Model;

use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class PontoPercurso implements InputFilterAwareInterface
{

    public $id;

    public $nome;

    public $descricao;

    public $local;

    public $gps_lat;

    public $gps_long;
    
    public $hashtags;
    
    public $disponibilidade;
    
    public $acessibilidade;
    
    public $tempoVisita;
    
    public $validado;

    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->id = (! empty($data['id'])) ? $data['id'] : null;
        $this->nome = (! empty($data['nome'])) ? $data['nome'] : null;
        $this->local = (! empty($data['local'])) ? $data['local'] : null;
        $this->descricao = (! empty($data['descricao'])) ? $data['descricao'] : null;
        $this->gps_lat = (! empty($data['gps_lat'])) ? $data['gps_lat'] : null;
        $this->gps_long = (! empty($data['gps_long'])) ? $data['gps_long'] : null;
        $this->hashtags=(! empty($data['hashtags'])) ? $data['hashtags'] : null;
        $this->disponibilidade=(! empty($data['disponibilidade'])) ? $data['disponibilidade'] : null;
        $this->acessibilidade=(! empty($data['acessibilidade'])) ? $data['acessibilidade'] : null;
        $this->validado=(! empty($data['validado'])) ? $data['acessibilidade'] : null;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        if (! $this->inputFilter) {
            $inputFilter = new InputFilter();
            $inputFilter->add(array(
                'name' => 'id',
                'required' => true,
                'filters' => array(
                    array(
                        'name' => 'Int'
                    )
                )
            ));
            $this->inputFilter = $inputFilter; // APAGAR ISTO
        }
        return $this->inputFilter;
    }
}

