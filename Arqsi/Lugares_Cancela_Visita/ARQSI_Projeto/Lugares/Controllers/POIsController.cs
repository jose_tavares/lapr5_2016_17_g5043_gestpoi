﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DataAccessLibrary.DAL;
using DataAccessLibrary.Models;
using Microsoft.AspNet.Identity;
using System.Diagnostics;

namespace NewNewLugares.Controllers
{
    [Authorize(Roles = "Editor")]
    public class POIsController : Controller
    {
        private DatumDbContext db = new DatumDbContext();

        private List<Hashtag> HashTemp;

        // GET: POIs
        public ActionResult Index()
        {
            var pOIs = db.POIs.Include(p => p.Local).Include(p => p.User).Include(p => p.hashtags);
            /*Process.Start(@"C:\Users\RazorTH\Documents\Prolog\file_test.pl");
            System.Threading.Thread.Sleep(5000);
            string text = System.IO.File.ReadAllText(@"C: \Users\RazorTH\Documents\Prolog\file.txt");*/
            return View(pOIs.ToList());
        }

        // GET: POIs/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            POI pOI = db.POIs.Find(id);
            if (pOI == null)
            {
                return HttpNotFound();
            }
            return View(pOI);
        }

        // GET: POIs/Create
        public ActionResult Create()
        {
            ViewBag.LocalID = new SelectList(db.Locals, "LocalID", "Name");
            ViewBag.UserId = new SelectList(db.Users, "Id", "Email");
            ViewBag.Hashtags = db.Hashtags.ToList().GroupBy(x => x.Name).Select(x => x.First());
            return View();
        }

        // POST: POIs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "POIID,Name,Description,LocalID,UserId,tempoVisita,acessivel")] POI pOI)
        {
            pOI.UserId = User.Identity.GetUserId();
            List<string> selectedHash = new List<string>();
            if (Request["Hashtags"] != null)
            {
                selectedHash = Request["Hashtags"].ToString().Split(',').ToList();
            }
            List<string> newHash = Request["Hashtags_New"].ToString().Split(',').ToList();
            string acessivel = Request["Acessivel_Check"];
            if (acessivel.Equals("false"))
            {
                pOI.accessivel = false;
            }
            else
            {
                pOI.accessivel = true;
            }
            pOI.validado = true;
            pOI.hashtags = new List<Hashtag>();
            foreach (string hash in selectedHash)
            {
                pOI.hashtags.Add(new Hashtag() { Name = hash } );
            }
            foreach (string hash in newHash)
            {
                if(!selectedHash.Contains(hash) && !hash.Equals(""))
                    pOI.hashtags.Add(new Hashtag() { Name = hash });
            }

            List<string> checks = FillChecks();
            int daycounter = 2;
            int timecounter = 0;
           pOI.dias = new List<Disponibilidade>();
            foreach(string day in checks)
            {
                if (day == "true")
                {
                    pOI.dias.Add(new Disponibilidade() { Day = (Dias)daycounter, TimeOfDay = (Tempos)timecounter });
                }
                timecounter++;
                if (timecounter > 2)
                {
                    daycounter++;
                    timecounter = 0;
                }
            }

            if (ModelState.IsValid)
            {
                db.POIs.Add(pOI);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            
            ViewBag.LocalID = new SelectList(db.Locals, "LocalID", "Name", pOI.LocalID);
            ViewBag.UserId = new SelectList(db.Users, "Id", "Email", pOI.UserId);
            ViewBag.Hashtags = db.Hashtags.ToList().GroupBy(x => x.Name).Select(x => x.First());
            return View(pOI);
        }

        private List<string> FillChecks()
        {
            List<string> checks = new List<string>();
            checks.Add(Request["Segunda_Manha"]);
            checks.Add(Request["Segunda_Tarde"]);
            checks.Add(Request["Segunda_Noite"]);
            checks.Add(Request["Terça_Manha"]);
            checks.Add(Request["Terça_Tarde"]);
            checks.Add(Request["Terça_Noite"]);
            checks.Add(Request["Quarta_Manha"]);
            checks.Add(Request["Quarta_Tarde"]);
            checks.Add(Request["Quarta_Noite"]);
            checks.Add(Request["Quinta_Manha"]);
            checks.Add(Request["Quinta_Tarde"]);
            checks.Add(Request["Quinta_Noite"]);
            checks.Add(Request["Sexta_Manha"]);
            checks.Add(Request["Sexta_Tarde"]);
            checks.Add(Request["Sexta_Noite"]);
            checks.Add(Request["Sabado_Manha"]);
            checks.Add(Request["Sabado_Tarde"]);
            checks.Add(Request["Sabado_Noite"]);
            checks.Add(Request["Domingo_Manha"]);
            checks.Add(Request["Domingo_Tarde"]);
            checks.Add(Request["Domingo_Noite"]);
            for (int i=0;i<checks.Count;i++)
            {
                if (checks[i].Equals("true,false")){
                    checks[i] = "true";
                }
            }
            return checks;
        }

        // GET: POIs/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            POI pOI = db.POIs.Find(id);
            if (pOI == null)
            {
                return HttpNotFound();
            }

            if (pOI.CheckNotOwner(User.Identity.GetUserId()))
            {
                return new HttpStatusCodeResult(HttpStatusCode.Unauthorized);
            }

            ViewBag.LocalID = new SelectList(db.Locals, "LocalID", "Name", pOI.LocalID);
            ViewBag.UserId = new SelectList(db.Users, "Id", "Email", pOI.UserId);

            //ViewBag.Hashtags = new SelectList(db.Hashtags, "Name", "Name").GroupBy(x => x.Text).Select(x => x.First());
            ViewBag.Hashtags = db.Hashtags.ToList().GroupBy(x => x.Name).Select(x => x.First());
            ViewBag.POIHashtags = pOI.hashtags.ToList();
            ViewBag.Disp = pOI.dias.ToList();
            ViewBag.AllDisp = db.Disponibilidades.ToList();
            HashTemp = pOI.hashtags.ToList();
            //db.Hashtags.RemoveRange(pOI.hashtags);
            //db.Disponibilidades.RemoveRange(pOI.dias);
            //db.SaveChanges();
            return View(pOI);
        }

        // POST: POIs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "POIID,Name,Description,LocalID,UserId,tempoVisita,acessivel")] POI pOI)
        {
            List<Hashtag> hashtemp = HashTemp;
            List<string> selectedHash = new List<string>();
            if (Request["Hashtags"] != null)
            {
                selectedHash = Request["Hashtags"].ToString().Split(',').ToList();
            }
            List<string> newHash = Request["Hashtags_New"].ToString().Split(',').ToList();
            string acessivel = Request["Acessivel_Check"];
            string validado = Request["Validado_Check"];
            if (acessivel.Equals("false"))
            {
                pOI.accessivel = false;
            }
            else
            {
                pOI.accessivel = true;
            }
            if (validado.Equals("false"))
            {
                pOI.validado = false;
            }
            else
            {
                pOI.validado = true;
            }
            pOI.validado = true;
            pOI.hashtags = new List<Hashtag>();
            Hashtag Hash = db.Hashtags.ToList().Last();
            int hashcounter = Hash.HashtagID;
            foreach (string hash in selectedHash)
            {
                hashcounter++;
                Hashtag h = new Hashtag() { HashtagID = hashcounter, Name = hash };
                pOI.hashtags.Add(h);
                db.Hashtags.Add(h);
            }
            foreach (string hash in newHash)
            {
                hashcounter++;
                if (!selectedHash.Contains(hash) && !hash.Equals(""))
                {
                    Hashtag h = new Hashtag() { HashtagID = hashcounter, Name = hash };
                    pOI.hashtags.Add(h);
                    db.Hashtags.Add(h);
                }
            }

            List<string> checks = FillChecks();
            int daycounter = 2;
            int timecounter = 0;
            Disponibilidade disp = db.Disponibilidades.ToList().Last();
            int dispcounter = disp.DisponibilidadeID;
            pOI.dias = new List<Disponibilidade>();
            foreach (string day in checks)
            {
                if (day == "true")
                {
                    dispcounter++;
                    Disponibilidade d = new Disponibilidade() { DisponibilidadeID = dispcounter, Day = (Dias)daycounter, TimeOfDay = (Tempos)timecounter };
                    pOI.dias.Add(d);
                    db.Disponibilidades.Add(d);
                }
                timecounter++;
                if (timecounter > 2)
                {
                    daycounter++;
                    timecounter = 0;
                }
            }
            if (ModelState.IsValid)
            {
                POI poiTest = db.POIs.Find(pOI.POIID);
                if (poiTest.CheckNotOwner(User.Identity.GetUserId()))
                {
                    return new HttpStatusCodeResult(HttpStatusCode.Unauthorized);
                }
                db.Hashtags.RemoveRange(poiTest.hashtags);
                db.Disponibilidades.RemoveRange(poiTest.dias);
                db.SaveChanges();
                foreach(Hashtag h in pOI.hashtags)
                {
                    poiTest.hashtags.Add(h);
                }
                foreach (Disponibilidade d in pOI.dias)
                {
                    poiTest.dias.Add(d);
                }
                db.Entry(poiTest).State = EntityState.Detached;

                pOI.UserId = User.Identity.GetUserId();
                db.Entry(pOI).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.LocalID = new SelectList(db.Locals, "LocalID", "Name", pOI.LocalID);
            ViewBag.UserId = new SelectList(db.Users, "Id", "Email", pOI.UserId);
            ViewBag.Hashtags = db.Hashtags.ToList().GroupBy(x => x.Name).Select(x => x.First());
            return View(pOI);
        }

        // GET: POIs/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            POI pOI = db.POIs.Find(id);
            if (pOI == null)
            {
                return HttpNotFound();
            }

            if (pOI.CheckNotOwner(User.Identity.GetUserId()))
            {
                return new HttpStatusCodeResult(HttpStatusCode.Unauthorized);
            }
            return View(pOI);
        }

        // POST: POIs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            POI pOI = db.POIs.Find(id);
            if (pOI.CheckNotOwner(User.Identity.GetUserId()))
            {
                return new HttpStatusCodeResult(HttpStatusCode.Unauthorized);
            }
            db.Hashtags.RemoveRange(pOI.hashtags);
            db.Disponibilidades.RemoveRange(pOI.dias);
            List<POIPercurso> poiperc = new List<POIPercurso>();
            foreach (POIPercurso pp in db.POIPercursos.ToList())
            {
                if (pOI.POIID == pp.POI.POIID)
                {
                    poiperc.Add(pp);
                }
            }
            db.POIPercursos.RemoveRange(poiperc);
            db.POIs.Remove(pOI);
           
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
