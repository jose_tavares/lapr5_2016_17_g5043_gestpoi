﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DataAccessLibrary.DAL;
using DataAccessLibrary.Models;
using Microsoft.AspNet.Identity;
using System.Data.Entity.Core;

namespace NewNewLugares.Controllers
{
    [Authorize(Roles = "Editor")]
    public class PercursosController : Controller
    {
        private DatumDbContext db = new DatumDbContext();

        // GET: Percursos
        public async Task<ActionResult> Index()
        {
            var percursos = db.Percursos.Include(p => p.User);
            if (System.IO.File.Exists("C:\\Agenda_Turista.txt"))
            {
                string text = System.IO.File.ReadAllText("C:\\Agenda_Turista.txt");
                Agenda nova = new Agenda();
                foreach (Agenda a in db.Agendas.ToList())
                {
                    if (a.Nome.Equals("Agenda_Temp"))
                    {
                        nova = a;
                    }
                }
                List<Visita> visitas = new List<Visita>();
                string[] t_visitas = text.Split(',');
                for (int i = 0; i < t_visitas.Length; i++)
                {
                    t_visitas[i] = t_visitas[i].Replace("[", "");
                    t_visitas[i] = t_visitas[i].Replace("]", "");
                }
                for (int i = 0; i < t_visitas.Length - 4; i += 4)
                {
                    POI p = db.POIs.Find(Int32.Parse(t_visitas[i + 1]));
                    Dias d = (Dias)Int32.Parse(t_visitas[i + 2]);
                    int t = Int32.Parse(t_visitas[i + 3]);
                    visitas.Add(new Visita() { UserId = t_visitas[i], poi = p, dia = d, hora = t });
                }
                nova.lVisitas = visitas;
                nova.Nome = "Agenda de " + nova.User.UserName;
                db.Visitas.AddRange(visitas);
                db.SaveChanges();
                System.IO.File.Delete("C:\\Agenda_Turista.txt");
            }
            return View(await percursos.ToListAsync());
        }

        // GET: Percursos/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Percurso percurso = await db.Percursos.FindAsync(id);
            List<POI> pois = new List<POI>();
            foreach(POIPercurso p in db.POIPercursos.ToList())
            {
                if (p.Percurso.percursoID == percurso.percursoID)
                {
                    pois.Add(p.POI);
                }
            }
            percurso.listaPoi = pois;
            if (percurso == null)
            {
                return HttpNotFound();
            }
            return View(percurso);
        }

        // GET: Percursos/Create
        public ActionResult Create()
        {
            ViewBag.percursoID = new SelectList(db.POIs, "POIID", "Name");
            ViewBag.POIs = db.POIs.ToList();
            //ViewBag.UserId = new SelectList(db.ApplicationUsers, "Id", "Email");
            return View();
        }

        // POST: Percursos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "percursoID,UserId,tempoPe,tempoCarro,kmTotal,tempoPlaneado,tempoViagem")] Percurso percurso)
        {
            percurso.UserId = User.Identity.GetUserId();
            List<POIPercurso> poiP = new List<POIPercurso>();
            percurso.Name = Request["Nome"].ToString();
            foreach(POI p in db.POIs.ToList()) {
                if (Request[p.Name] != null)
                {
                    string a = Request[p.Name].ToString();
                    if (!a.Equals("false"))
                    {
                        poiP.Add( new POIPercurso()
                        {
                            POI = p,
                            Percurso = percurso,
                        });
                    }
                }
            }
            db.POIPercursos.AddRange(poiP);
            if (!Request["Definido_Check"].ToString().Equals("false"))
            {
                percurso.predefinido = true;
            }

            string tempo = Request["Tempo"].ToString();

            percurso.tempoViagem = Int32.Parse(tempo);
            
            if (ModelState.IsValid)
            {
                db.Percursos.Add(percurso);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.percursoID = new SelectList(db.POIs, "POIID", "Name", percurso.percursoID);
            ViewBag.POIs = db.POIs.ToList();
            // ViewBag.UserId = new SelectList(db.ApplicationUsers, "Id", "Email", percurso.UserId);
            return View(percurso);
        }

        // GET: Percursos/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Percurso percurso = await db.Percursos.FindAsync(id);
            if (percurso == null)
            {
                return HttpNotFound();
            }
            ViewBag.percursoID = new SelectList(db.POIs, "POIID", "Name", percurso.percursoID);
            ViewBag.POIs = db.POIs.ToList();
            List<POI> pois = new List<POI>();
            foreach (POIPercurso p in db.POIPercursos.ToList())
            {
                if (p.Percurso.percursoID == percurso.percursoID)
                {
                    pois.Add(p.POI);
                }
            }
            percurso.listaPoi = pois;
            ViewBag.Nome = percurso.Name;
            // ViewBag.UserId = new SelectList(db.ApplicationUsers, "Id", "Email", percurso.UserId);
            return View(percurso);
        }

        // POST: Percursos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "percursoID,UserId,tempoPe,tempoCarro,kmTotal,tempoPlaneado,tempoViagem")] Percurso percurso)
        {
            percurso.UserId = User.Identity.GetUserId();
            List<POIPercurso> poiP = new List<POIPercurso>();
            percurso.Name = Request["Nome"].ToString();
            foreach (POI p in db.POIs.ToList())
            {
                if (Request[p.Name] != null)
                {
                    string a = Request[p.Name].ToString();
                    if (!a.Equals("false"))
                    {
                        poiP.Add(new POIPercurso()
                        {
                            POI = p,
                            Percurso = percurso,
                        });
                    }
                }
            }
            //db.POIPercursos.AddRange(poiP);
            if (!Request["Definido_Check"].ToString().Equals("false"))
            {
                percurso.predefinido = true;
            }

            string tempo = Request["Tempo"].ToString();

            percurso.tempoViagem = Int32.Parse(tempo);
            percurso.listaPoi = new List<POI>();
            if (ModelState.IsValid)
            {
                Percurso perTest = db.Percursos.Find(percurso.percursoID);
                if (perTest.CheckNotOwner(User.Identity.GetUserId()))
                {
                    return new HttpStatusCodeResult(HttpStatusCode.Unauthorized);
                }
                List<POIPercurso> poipercursos = new List<POIPercurso>();
                foreach (POIPercurso pp in db.POIPercursos.ToList())
                {
                    if (pp.Percurso.percursoID == perTest.percursoID)
                    {
                        poipercursos.Add(pp);
                    }
                }
                db.POIPercursos.RemoveRange(poipercursos);
                db.POIPercursos.AddRange(poiP);
                //perTest.listaPoi = new List<POI>();
                //db.Percursos.Remove(perTest);
                db.SaveChanges();



                //db.Entry(perTest).State = EntityState.Detached;
                percurso.UserId = User.Identity.GetUserId();
                db.Entry(percurso).State = EntityState.Modified;
                //db.Percursos.Add(percurso);
                await db.SaveChangesAsync();
                db.Percursos.Remove(perTest);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.percursoID = new SelectList(db.POIs, "POIID", "Name", percurso.percursoID);
           // ViewBag.UserId = new SelectList(db.ApplicationUsers, "Id", "Email", percurso.UserId);
            return View(percurso);
        }

        // GET: Percursos/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Percurso percurso = await db.Percursos.FindAsync(id);
            List<POI> pois = new List<POI>();
            foreach (POIPercurso p in db.POIPercursos.ToList())
            {
                if (p.Percurso.percursoID == percurso.percursoID)
                {
                    pois.Add(p.POI);
                }
            }
            percurso.listaPoi = pois;
            if (percurso == null)
            {
                return HttpNotFound();
            }
            return View(percurso);
        }

        // POST: Percursos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Percurso percurso = await db.Percursos.FindAsync(id);
            List<POIPercurso> poipercursos = new List<POIPercurso>();
            foreach (POIPercurso pp in db.POIPercursos.ToList())
            {
                if (pp.Percurso.percursoID == percurso.percursoID)
                {
                    poipercursos.Add(pp);
                }
            }
            db.POIPercursos.RemoveRange(poipercursos);
            //percurso.listaPoi.Clear();
            db.Percursos.Remove(percurso);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
