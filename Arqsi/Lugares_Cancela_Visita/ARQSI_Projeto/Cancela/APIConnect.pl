:- use_module(library(http/http_open)).
:- use_module(library(http/json)).
%:- use_module(library(http/json_convert)).
:- use_module(library(http/http_ssl_plugin)).
:-initialization(run).
:- dynamic poi/2.
:- dynamic disppoi/4.
:- dynamic dispuser/4.
:- dynamic disp_turista/8.
:- dynamic disp_imovel/4.
:- dynamic poiid/1.
:- dynamic userid/1.
:- dynamic infoturista/5.
:- dynamic infopoi/5.
:- dynamic horas/2.
:- dynamic imovel/4.
:- dynamic distancia/5.
:- dynamic turista/2.

url(locais,'http://localhost:13097/api/Locals?token=TukPorto').
url(pois,'http://10.8.11.93:443/CancelaW/api/Agenda/-1').
url(test,'http://10.8.11.93:443/CancelaW/api/POIs').
token('Bearer RzYSvRiyEMyiKvRalA3HQONN9gOEIur88pvSNemPr42kjz34FU57dFranilPXMSgyKuZeRUq_h-teUwe__FpXMekCEMSjUR0OAsZw16H57eblFrflyU2OIwH4E8Mr5GXeEdZX2bLT2ydYlNtj4q6LcUivZ7kwYpxLYsG9xlAMw2SfuwwefmeyNyJGEBXJSfsWStrC-iFs02qwk8MA00PJwLWNt4ZWAD6dQ6ZeHIPmZ0AvQ6o4AD4svxjqxyNtvg9V8sAukAWjOjhMZEHQ3S10IDzMcKrqHBL7f5kPcQ4FKOiWXtVGC0Kz7kneoj0f-QMdBNyeot9kvRp1c2wUHo68l0gfWMpRZYaT_kYpqqL2iHuxKCsvIkQ6JsMSdrBoileNdHZD5lIWTCsmyesNx0HyrzZ0RLMJnMb-A-GrkjIYKCpr7mHK5Ldl2ubp_B3JNF6nI18ueuM6bXmbgSlS2x6or1DRcDloi-VmhAedZJZgveFFGzJaBzGsPqItTnMelImnjcq7hYQ1C5cJ0Pa6wjoxEgcw3mAukxPorU5cyLCf0M').

viagem(carro,40).
viagem(pe,3).
distancia_cal(X1,Y1,X2,Y2,Res1):-
	atom_number(X1,X11),
	atom_number(Y1,Y11),
	atom_number(X2,X22),
	atom_number(Y2,Y22),
	XX is (X22-X11)^2,
	YY is (Y22-Y11)^2,
	Res is sqrt(XX+YY),
	Res1 is round(Res).

calcula_tempo(X,D,Res):-
	viagem(X,Y),
	Res1 is Y*D,
	Res is round(Res1).

api(URL,Dict):-
	token(X),
  setup_call_cleanup(
    http_open(URL, In, [request_header('Accept'='application/json'),request_header('Authorization'=X)]),
    json_read_dict(In, Dict),
    close(In)
  ).

jsonTerm(JSON):-
	url(pois,URL),
	api(URL,JSON).

run():-
	do_list(18,26,L),
	do_list(26,36,L2),
	do_list(36,46,L3),
	assertz(horas(0,L)),
	assertz(horas(1,L2)),
	assertz(horas(2,L3)),
	get_turista(),
	divide_json_ld(),
	createHorasTurista(),
	get_all_pois(),
	calcularDist(),
	create().
	
create():-
	open('file.txt',write, Stream),
	write(Stream,'stuff'),
	close(Stream),
	halt().

get_turista():-
	retractall(poiid),
	retractall(infoturista(_,_,_,_,_)),
	jsonTerm(L),
	with_output_to(atom(All),write(L)),
	sub_string(All,A,_,_,"userID"),
	atom_length(All,B),
	A1 is A+7,
	B1 is B-1,
	AB is B1-A1,
	sub_string(All,A1,AB,_,L1),
	split_string(L1,":,","",L2),
	criarUserID(L2).

divide_json_ld():-
	retractall(dispuser(_,_,_,_)),
	jsonTerm(L),
	with_output_to(atom(All),write(L)),
	sub_string(All,A,_,_,"ld:"),
	sub_string(All,B,_,_,"pois"),
	AB is B-A,
	sub_string(All,A,AB,_,LD),
	sub_string(LD,A1,_,_,"{"),
	sub_string(LD,B1,_,_,"}]"),
	A1B1 is (B1+1)-A1,
	sub_string(LD,A1,A1B1,_,LD1),
	split_string(LD1,"{:,}","",LD2),
	removeFirst(LD2,LD3),
	removeLast(LD3,LD4),
	pair_list(LD4,LD5),
	criarDispUser(LD5).

get_all_pois():-
	jsonTerm(L),
	with_output_to(atom(All),write(L)),
	sub_string(All,A,_,_,"pois"),
	sub_string(All,B,_,_,"}],user"),
	A1 is A+11,
	B1 is B+1,
	AB is B1-A1,
	sub_string(All,A1,AB,_,L2),
	createPoisList(L2,_).

createPoisList(L,[L3|L2]):-
	sub_string(L,A,_,_,"{Desc"),
	sub_string(L,B,_,_,"validado"),
	B11 is B+8,
	AB is B11-A,!,
	AB >= 0,
	sub_string(L,A,AB,_,L3),
	atom_length(L,B1),!,
	BB is B1-B11,
	BB >= 0,
	sub_string(L,B11,BB,_,L4),
	get_pois(L3),
	createPoisList(L4,L2).

createPoisList(_,_).

get_pois(L2):-
	get_poi_id(L2,ID),
	get_poi_lat(L2,Lat),
	get_poi_long(L2,Long),
	get_poi_acc(L2,Acc),
	get_poi_visita(L2,Visita),
	retractall(poiid(_)),
	assertz(poiid(ID)),
	assertz(poi(ID,ID)),
	%retractall(infopoi(_,_,_,_,_)),
	assertz(infopoi(ID,Lat,Long,Acc,Visita)),
	assertz(imovel(ID,ID,_,Visita)),
	get_poi_disp(L2,Disp1),
	Disp = ['',''|Disp1],
	pair_list(Disp,Disp2),
	removeLast(Disp2,Disp3),
	criarDispPOI(Disp3),
	createHorasPOI().

get_poi_id(L,V):-
	sub_string(L,A,_,_,"POIID"),
	sub_string(L,B,_,_,"User"),
	A1 is A+1,
	B1 is B-1,
	AB is B1-A1,
	sub_string(L,A1,AB,_,L2),
	split_string(L2,":","",[_,V]).

get_poi_lat(L,V):-
	sub_string(L,A,_,_,"GPS_Lat"),
	sub_string(L,B,_,_,",GPS_Long"),
	A1 is A+1,
	AB is B-A1,
	sub_string(L,A1,AB,_,L2),
	split_string(L2,":","",[_,V]).

get_poi_long(L,V):-
	sub_string(L,A,_,_,"GPS_Long"),
	sub_string(L,B,_,_,",LocalID"),
	A1 is A+1,
	AB is B-A1,
	sub_string(L,A1,AB,_,L2),
	split_string(L2,":","",[_,V]).

get_poi_acc(L,V):-
	sub_string(L,A,_,_,"accessivel"),
	sub_string(L,B,_,_,",dias"),
	A1 is A+1,
	AB is B-A1,
	sub_string(L,A1,AB,_,L2),
	split_string(L2,":","",[_,V]).

get_poi_visita(L,V):-
	sub_string(L,A,_,_,"tempoVisita"),
	sub_string(L,B,_,_,",validado"),
	A1 is A+1,
	AB is B-A1,
	sub_string(L,A1,AB,_,L2),
	split_string(L2,":","",[_,V]).

get_poi_disp(L,V):-
	sub_string(L,A,_,_,"dias"),
	sub_string(L,B,_,_,"hashtags"),
	A1 is A+1,
	B1 is B-1,
	AB is B1-A1,
	sub_string(L,A1,AB,_,L2),
	sub_string(L2,AA,_,_,"{"),
	sub_string(L2,BB,_,_,"}]"),
	BB1 is BB+1,
	AABB is BB1-AA,
	sub_string(L2,AA,AABB,_,V2),
	split_string(V2,"{:,}","",V3),
	removeFirst(V3,V4),
	removeLast(V4,V).

pair_list([],[(_,_)]).

pair_list([H,T|L],[(H,T)|L2]):-
	pair_list(L,L2).

criarUserID([U,_,A,_,Carro,_,KM,_,Pe,_,Visita]):-
	assertz(userid(U)),
	assertz(turista(U,_)),
	assertz(infoturista(A,Carro,KM,Pe,Visita)).

criarDispUser([]).
criarDispUser([(_,A),(_,B),(_,C),(_,D)|L]):-
	userid(D),
	assertz(dispuser(A,B,C,D)),
	criarDispUser(L).

criarDispPOI([]).
criarDispPOI([(_,_),(_,A),(_,B),(_,C)|L]):-
	poiid(D),
	assertz(disppoi(A,B,C,D)),
	criarDispPOI(L).

getAllDispUser(L):-
	findall([Dia,H],dispuser(Dia,_,H,_),L).

getAllDispPOI(L):-
	findall([Dia,H],disppoi(Dia,_,H,_),L).

createHorasPOI():-
	getAllDispPOI(L),
	createHorasP(L,_).

createHorasPOI([]).

createHorasPOI([H,T|L]):-
	createHorasPOI(L),
	poiid(ID),
	infopoi(ID,_,_,Acc,_),
	((Acc=="true",A1 is 1);(Acc=="false",A1 is 0)),
	assertz(disp_imovel(ID,H,T,A1)).

createHorasTurista():-
	getAllDispUser(L),
	createHoras(L,_).

createHorasTurista([]).

createHorasTurista([H,T|L]):-
	createHorasTurista(L),
	userid(ID),
	infoturista(A,Carro,KM,Pe,Visita),
	((A=="true",A1 is 1);(A=="false",A1 is 0)),
	assertz(disp_turista(ID,H,T,A1,Carro,KM,Pe,Visita)).

createHoras([],LL):-
	createHorasTurista(LL).

createHoras([[D,H]|L],L2):-
	atom_number(D,Dia),
	atom_number(H,Hora),
	horas(Hora,Horas),
	createHora(Dia,Horas,L3),
	append(L3,L2,LL),
	createHoras(L,LL).

createHorasP([],LL):-
	createHorasPOI(LL).

createHorasP([[D,H]|L],L2):-
	atom_number(D,Dia),
	atom_number(H,Hora),
	horas(Hora,Horas),
	createHora(Dia,Horas,L3),
	append(L3,L2,LL),
	createHorasP(L,LL).

createHora(_,[],[]).

createHora(Dia,[Hora|T],[Dia,Hora|L]):-
	createHora(Dia,T,L).

getAllPoi(L2):-
	findall([ID,Lat,Long],infopoi(ID,Lat,Long,_,_),L2).

calcularDist():-
	getAllPoi(L),
	calcularDist(L).

calcularDist([]).
calcularDist([_]).
calcularDist([[ID1,X1,Y1],[ID2,X2,Y2]|L]):-
	calcularDist([[ID2,X2,Y2]|L]),
	distancia_cal(X1,Y1,X2,Y2,V),
	calcula_tempo(carro,V,V1),calcula_tempo(pe,V,V2),
	assertz(distancia(ID1,ID2,V,V1,V2)).



do_list(N1,N2, L):-
  findall(Num, between(N1, N2, Num), L).



removeFirst([_|L],L).

removeLast([_], []).
removeLast([X|Xs], [X|WithoutLast]) :-
    removeLast(Xs, WithoutLast).

