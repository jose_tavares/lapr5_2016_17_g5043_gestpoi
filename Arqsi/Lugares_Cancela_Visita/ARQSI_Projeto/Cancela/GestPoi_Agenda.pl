%carregar ficheiros do mapa.
%carrega_mapa('mapa.txt').
%(NOTA: o ficheiro do mapa tem que estar na mesma diretoria do prolog).
:- consult(apiconnect).
%carrega_mapa(M):-
	%see(M),
	%repeat,
	%read(X),
	%processa_mapa(X),
	%X==end_of_file,
	%!,
	%seen.

%processa_mapa(end_of_file).
%processa_mapa(X):-assert(X).

%carregar ficheiros de disponibilidades.
%carrega_dis('disponibilidades.txt').
%(NOTA: o ficheiro das disponibilidades tem que estar na mesma diretoria do prolog).

%carrega_dis(D):-
	%see(D),
	%repeat,
	%read(Z),
	%processa_dis(Z),
	%Z==end_of_file,
	%!,
	%seen.

%processa_dis(end_of_file).
%processa_dis(Z):-assert(Z).

% Output da agenda do Turista.
% Este metodo gera um ficheiro (Agenda_Turista.txt) com a agenda do Turista.
%[Agenda],Distancia inicio ao fim, Duracao em minutos da Viagem, Duracao com visitas

agenda_turista(Turista) :-
	agenda_turista(Turista, Agenda,ViajAB, PontoInicial,Duracao),
	PI = [Turista, PontoInicial, _, _],
	validar_agenda_limite_km(Turista,Agenda,ViajAB, PI, Duracao, Total, Limitekm, TempoTotalVisita),
	validar_agenda_visitas_programadas(Turista,Agenda,TempoTotalVisita,Programada,TempoTotalVisitaFinal),
	write(Agenda), nl,
	(   var(ViajAB) -> ViajAB is 0,
	    var(Total) -> Total is 0,
	    var(TempoTotalVisita) -> TempoTotalVisita is 0,
	    var(TempoTotalVisitaFinal) -> TempoTotalVisitaFinal is 0,
	    disp_turista(Turista,_,_,_,Limitekm,_,_,Programada),
        write('Distancia percorrida = '),write(Total), write(' Km'),nl,
        write('Limite km do Turista = '), write(Limitekm), write(' Km'),nl,
	write('Visita de '), write(Programada), write(' Horas escolhida pelo Turista.'),nl,
	write('Duracao da viajem = '), write(TempoTotalVisita), write(' minutos'),nl,
	write('Duracao da Viajem com visitas = '), write(TempoTotalVisitaFinal), write(' minutos'),nl,
	    open('Agenda_Turista.txt',append,X),
	    write(X,Agenda), write(X,','),
	    write(X,Total), write(X,','),
	    write(X,TempoTotalVisita), write(X,','),
	    write(X,TempoTotalVisitaFinal), write(X,','),
	    close(X),
	    nl;
	write('Distancia = '),write(Total), write(' Km'),nl,
	write('Limite km do Turista = '), write(Limitekm), write(' Km'),nl,
	write('Visita de '), write(Programada), write(' Horas escolhidas pelo Turista.'),nl,
	write('Duracao da viajem = '), write(TempoTotalVisita), write(' minutos'),nl,
	write('Duracao da viajem com visitas = '), write(TempoTotalVisitaFinal), write(' minutos'),nl,
	    open('Agenda_Turista.txt',append,X),
	    write(X,Agenda), write(X,','),
	    write(X,Total), write(X,','),
	    write(X,TempoTotalVisita), write(X,','),
	    write(X,TempoTotalVisitaFinal), write(X,','),
	    close(X),
	    nl).

agenda_turista(_).

% Criar a agenda do Turista
agenda_turista(Turista, Agenda, ViajAB, PontoInicial,Duracao) :-
	agenda_visitas(Turista, Visitas, PontoInicial),		% Imoveis que o turista quer visitar
	maplist(marcar_visita, Visitas, VisitasPossiveis),      % Visitas possiveis
	exclude(xvazio, VisitasPossiveis, NaoVazio),	        % Eliminar visitas vazias
        length(NaoVazio, A), A > 0,
	ordenar_agenda(NaoVazio, Agenda),		% Ordenar visitas
	validar_agenda(Turista, Agenda,ViajAB,Duracao).

%Se nao existir agenda possivel
agenda_turista(_,[]).

last(X,[X]).
last(X,[_|L]):-last(X,L).

% Criar visitas para o Turista (funcional).
agenda_visitas(Turista, Visitas, PontoInicial) :-
	setof([Turista, P], visita(Turista, P, PontoInicial), Visitas).

%imovel que o turista visita (funcional).
visita(Turista, Imovel, PontoInicial) :-
	visita_imovel(Turista, ListaImoveis, PontoInicial),
	member(Imovel, ListaImoveis).

% Compatibilidade de visita entre as disponibilidades do turista e do poi
% (imovel).(funcional).

marcar_visita([Turista, Imovel], [Turista, Imovel, Dia, Hora]) :-
	disp_turista(Turista, Dia, Hora, Acessibilidade, LimiteKm, _, _,_),
	LimiteKm > 0, % se limite de km for 0 o turista nao pode ter agenda.
	(   Acessibilidade == 1 -> %se o Turista tem necessidades especiais entao,
	disp_imovel(Imovel, Dia, Hora, Acessibilidade); % O espaco tem que permitir necessidades especiais.
	disp_imovel(Imovel, Dia, Hora, _)). %Senao, o espa�o nao requer necessidades especiais.

% Nao ha visitas (funcional).
marcar_visita(_, []).

% Eliminar visitas vazias (funcional).
xvazio(Lista) :- length(Lista, 0).

% Ordenar agenda pelo iniciovisita, primeiro por dia, depois por hora
ordenar_agenda(NaoOrdenada, AgendaOrdenada) :-
	map_list_to_pairs(iniciovisita, NaoOrdenada, ParesTempo), % Mapear cada elemento da lista de listas com uma chave:Chave - Valor
	keysort(ParesTempo, Ordenado), % Ordenar a lista de pares
	pairs_values(Ordenado, AgendaOrdenada). % Eliminar as chaves da lista ordenada

% Calcular tempo de visita
iniciovisita([_Turista, _Imovel, Dia, Hora], Tempo) :-
	Dia1 is Dia - 1,
	DiaHr is Dia1 * 48,		% 48 1/2 hr por dia
	TempoHr is DiaHr + Hora,		% Visita no dia
	Tempo is TempoHr * 30.

% Agenda e valida? as visitas unicas podem sempre ser agendadas
validar_agenda(_,Agenda,_,_) :-
	length(Agenda, 1).

% Caso contrario, ha tempo suficiente para visita A e viajar ate B?
validar_agenda(Turista,[A, B | Agenda],ViajAB,Duracao) :-	% Agendar A para B
	iniciovisita(A, InicioA),		% Comeco das visitas
	iniciovisita(B, InicioB),
	tempovisita(A, VisitaA),		% Tempo para uma visita
	viajar(A,B,ViajAB,Duracao),		% Viajar de A a ate B
	sum_list([InicioA, VisitaA, ViajAB], ProxVisita),  % Proxima visita mais proxima
	ProxVisita < InicioB,		% Turista chega a proxima visita
	!,				% Qualquer rota dentro do tempo e boa
	validar_agenda(Turista,[B | Agenda],ViajAB,Duracao).	% Continua a verificar a agenda

%Agenda v�lida.
validar_agenda_limite_km(_,Agenda,_,_,_,_,_,_):-length(Agenda, 1).

%Validar Agenda pelo limite de km definidos pelo utilizador.
validar_agenda_limite_km(Turista, [A, B | Agenda], ViajAB, PI, Duracao, Total, Limitekm, DuracaoTotalViajem):-
	last(PF,[A, B|Agenda]),
	viajar(PI,PF,Distancia,DuracaoViajem),
	disp_turista(Turista,_,_,_,Limitekm,_,_,_),
	sum_list([Duracao, DuracaoViajem],DuracaoTotalViajem),
	sum_list([ViajAB, Distancia], Total),	%somar viajAB com Distancia
	Total =< Limitekm,                      %Se limite do utilizador maior que o resultado anterior
	!,					%qualquer trajeto dentro dos km e valido
	validar_agenda_limite_km(Turista,[B | Agenda], ViajAB, PI, Duracao, Total, Limitekm, DuracaoTotalViajem).%continua a verificar


validar_agenda_visitas_programadas(_,Agenda,_,_,_):-length(Agenda, 1).

validar_agenda_visitas_programadas(Turista,[A, B | Agenda], TempoTotalVisita, Programada, TempoTotalVisitaFinal):-
	tempovisita(A, VisitaA),                        %tempo que demora a visitar A.
	viajar(A, B,_,DuracaoViajem),			%tempo de viagem entre A e B.
	disp_turista(Turista,_,_,_,_,_,_,Programada),   %recebe por parametro se visita � de 4H ou 8H.
	Converte is Programada * 60,                    %converte as Horas em Minutos.
	sum_list([VisitaA,DuracaoViajem,TempoTotalVisita], TempoTotalVisitaFinal),         %Soma a Viagem com o tempo da visita.
	TempoTotalVisitaFinal =< Converte,                         %Verifica se Visita esta dentro dos limites.
	!,                                              %Qualquer percurso dentro do tempo estimado e boa.
	validar_agenda_visitas_programadas(Turista, [B | Agenda], TempoTotalVisita, Programada, TempoTotalVisitaFinal).


% Duracao de uma visita
tempovisita([_Turista, Imovel, _Dia, _Hora], TempoVisita) :-
	imovel(Imovel, _Poi, _Tipo, TempoVisita).

% Km de viagem entre visitas e duracao
viajar(A, B, ViajAB, DuracaoViajem) :-
	A = [_TuristaA, ImovelA, _, _],  % Imovel da visita A
	imovel(ImovelA, PoiA, _, _),
	B = [_TuristaB, ImovelB, _, _],  % Imovel da visita B
	imovel(ImovelB, PoiB, _, _),
	viajar([PoiB], PoiA, 0, ViajAB, 0, DuracaoViajem).

/*
   Viajar a rede de estradas em sentido inverso usando Primeiro em Profundidade.
   Em sentido inverso pois caso estivessemos interessados na rota, a sequencia estaria correta.
   Comecando pelo destino sao gradualmente acrescentados novos poi ate ser encontrada
   a origem. Percorrido, e a distancia percorrida ao criar a rota ate a sua posicao atual;
   Distancia sera a distancia final quando se alcansa a origem.

 */

% Encontrou origem A
viajar([A | _Rta], A, Distancia, Distancia, Duracao, Duracao).

% Viajar de B ate A
viajar([B | Rta], A, Percorrido, Distancia, Tempo, Duracao) :-
	estrada(C, B, Dist, _, TempoCarro),	   % Estrada de C ate B
	\+ member(C, [B | Rta]),	   % C nao pode estar na rota
	Total is Percorrido + Dist,	   % Adicionar distancia de C ate B
	%TotalPe is Tempo_Pe + TempoPe,
	TotalCarro is Tempo + TempoCarro,
	viajar([C, B | Rta], A, Total, Distancia, TotalCarro, Duracao).

% Estradas sao bidirecionais
estrada(C, B, Distancia, TempoPe, TempoCarro) :- distancia(C, B, Distancia, TempoPe, TempoCarro).
estrada(C, B, Distancia, TempoPe, TempoCarro) :- distancia(B, C, Distancia, TempoPe, TempoCarro).
