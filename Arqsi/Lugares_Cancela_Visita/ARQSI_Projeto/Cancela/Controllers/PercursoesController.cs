﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using DataAccessLibrary.DAL;
using DataAccessLibrary.Models;

namespace NewNewCancela.Controllers
{
    [Authorize(Roles = "Utilizador")]
    public class PercursoesController : ApiController
    {
        private DatumDbContext db = new DatumDbContext();

        // GET: api/Percursoes
        public IEnumerable<PercursoToSend> GetPercursos()
        {
            //List<POIPercurso> ppl = new List<POIPercurso>();
            //foreach(Percurso p in db.Percursos.ToList())
            //{
            //    List<POI> pl = new List<POI>();
            //    foreach (POIPercurso pp in db.POIPercursos.ToList())
            //    {
            //        if (pp.Percurso.percursoID == p.percursoID)
            //        {
            //            pl.Add(pp.POI);
            //        }
            //    }
            //    p.listaPoi = pl;
            //}
            //return db.Percursos;

            List<PercursoToSend> newList = new List<PercursoToSend>();
            IEnumerable<Percurso> list = db.Percursos.ToList();
            foreach (var p in list)
            {
                newList.Add(new PercursoToSend(p));
            }
            return newList;
        }

        // GET: api/Percursoes/5
        [ResponseType(typeof(Percurso))]
        public async Task<IHttpActionResult> GetPercurso(int id)
        {
            Percurso percurso = await db.Percursos.FindAsync(id);
            List<POI> p = new List<POI>();
            foreach(POIPercurso pp in db.POIPercursos.ToList())
            {
                if(pp.Percurso.percursoID == percurso.percursoID)
                {
                    p.Add(pp.POI);
                }
            }
            percurso.listaPoi = p;
            if (percurso == null)
            {
                return NotFound();
            }

            return Ok(percurso);
        }

        // PUT: api/Percursoes/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutPercurso(int id, Percurso percurso)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != percurso.percursoID)
            {
                return BadRequest();
            }

            db.Entry(percurso).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PercursoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Percursoes
        [ResponseType(typeof(Percurso))]
        public async Task<IHttpActionResult> PostPercurso(Percurso percurso)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Percursos.Add(percurso);
            List<POIPercurso> pp = new List<POIPercurso>();
            foreach(POI p in percurso.listaPoi)
            {
                pp.Add(new POIPercurso() { POI = p, Percurso = percurso });
            }
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = percurso.percursoID }, percurso);
        }

        // DELETE: api/Percursoes/5
        [ResponseType(typeof(Percurso))]
        public async Task<IHttpActionResult> DeletePercurso(int id)
        {
            Percurso percurso = await db.Percursos.FindAsync(id);
            if (percurso == null)
            {
                return NotFound();
            }

            db.Percursos.Remove(percurso);
            await db.SaveChangesAsync();

            return Ok(percurso);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PercursoExists(int id)
        {
            return db.Percursos.Count(e => e.percursoID == id) > 0;
        }
    }
}