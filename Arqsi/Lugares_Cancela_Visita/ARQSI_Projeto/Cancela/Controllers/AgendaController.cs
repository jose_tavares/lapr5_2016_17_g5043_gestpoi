﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using DataAccessLibrary.DAL;
using DataAccessLibrary.Models;
using Microsoft.AspNet.Identity;
using DataAccessLibrary.Infrastructure;
using System.Diagnostics;

namespace NewNewCancela.Controllers
{
    [Authorize(Roles = "Utilizador")]
    public class AgendaController : ApiController
    {
        private DatumDbContext db = new DatumDbContext();

        // GET: api/Agenda
        public IQueryable<Agenda> GetAgendas()
        {
            List<Agenda> agendas = new List<Agenda>();
            foreach(Agenda a in db.Agendas.ToList())
            {
                if (!a.Nome.Equals("Agenda_Temp"))
                {
                    agendas.Add(a);
                }
            }
            IQueryable<Agenda> age = agendas.AsQueryable();
            return age;
        }

        // GET: api/Agenda/5
        [ResponseType(typeof(Agenda))]
        public async Task<IHttpActionResult> GetAgenda(int id)
        {
            if (id == -1)
            {
                Agenda a = new Agenda();
                foreach (Agenda aa in db.Agendas.ToList())
                {
                    if (aa.Nome.Equals("Agenda_Temp"))
                    {
                        a = aa;
                    }
                }
                //Percurso p = new Percurso();
                //foreach(Agenda a1 in db.Agendas.ToList())
                //{
                //    if (a1.Percurso.Name.Equals("Agenda_Temp"))
                //    {
                //        p = a1.Percurso;
                //    }
                //}
                //a.Percurso = p;
                List<Disponibilidade> ld = new List<Disponibilidade>();
                foreach(UserDisp ud in db.userDisps.ToList())
                {
                    if(ud.userID == User.Identity.GetUserId())
                    {
                        ld = ud.dias;
                    }
                }
                AgendatoCreate ac = new AgendatoCreate(a,ld);
                return Ok(ac);
            }
            Agenda agenda = await db.Agendas.FindAsync(id);
            if (agenda == null)
            {
                return NotFound();
            }

            return Ok(agenda);
        }

        // PUT: api/Agenda/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutAgenda(int id, Agenda agenda)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != agenda.AgendaID)
            {
                return BadRequest();
            }

            db.Entry(agenda).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AgendaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Agenda
        [ResponseType(typeof(Agenda))]
        public async Task<IHttpActionResult> PostAgenda(Agenda agenda)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Agendas.Add(agenda);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = agenda.AgendaID }, agenda);
        }

        // POST: api/AgendaCreate
        [ResponseType(typeof(Agenda))]
        public async Task<IHttpActionResult> PostAgendaCreate(int percursoID, string username)
        {

            foreach(Agenda aa in db.Agendas.ToList())
            {
                if (aa.Nome.Equals("Agenda_Temp"))
                {
                    db.Agendas.Remove(aa);
                }
            }
            db.SaveChanges();
            string uID = "";
            foreach(ApplicationUser appu in db.Users.ToList())
            {
                if (appu.Email.Equals(username))
                {
                    uID = appu.Id;
                }
            }
            Agenda a = new Agenda() { Nome = "Agenda_Temp",UserId=uID, PercursoID = percursoID, lVisitas = new List<Visita>() };
            db.Agendas.Add(a);
            await db.SaveChangesAsync();
            Process.Start("C:\\GestPoi_Agenda.pl");
            //string text = System.IO.File.ReadAllText(@"C: \Users\RazorTH\Documents\Prolog\file.txt");

            return CreatedAtRoute("DefaultApi", new { id = a.PercursoID }, a);
        }

        // DELETE: api/Agenda/5
        [ResponseType(typeof(Agenda))]
        public async Task<IHttpActionResult> DeleteAgenda(int id)
        {
            Agenda agenda = await db.Agendas.FindAsync(id);
            if (agenda == null)
            {
                return NotFound();
            }

            db.Agendas.Remove(agenda);
            await db.SaveChangesAsync();

            return Ok(agenda);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool AgendaExists(int id)
        {
            return db.Agendas.Count(e => e.AgendaID == id) > 0;
        }
    }
}