﻿using NewVisita.Helpers;
using System;
using System.Globalization;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Web.Mvc;
using System.Web;

namespace NewVisita.Models
{

    public class LoggedIn : AuthorizeAttribute
    {
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            TokenResponse tokenResponse = WebApiHttpClient.getToken();

            var client = WebApiHttpClient.GetClient();
            var response = client.GetAsync("api/Account/TestToken/").Result;
            if (response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
            {
                return false;
            }

            return tokenResponse == null ? false : true;
        }

    }
}