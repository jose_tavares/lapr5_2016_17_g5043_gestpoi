namespace DataAccessLibrary.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Locals",
                c => new
                    {
                        LocalID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        GPS_Lat = c.Single(nullable: false),
                        GPS_Long = c.Single(nullable: false),
                    })
                .PrimaryKey(t => t.LocalID);
            
            CreateTable(
                "dbo.Meteorologies",
                c => new
                    {
                        MeteorologyID = c.Int(nullable: false, identity: true),
                        TimeOfReading = c.DateTime(nullable: false),
                        HourOfReading = c.DateTime(nullable: false),
                        Temperature = c.Single(nullable: false),
                        Wind = c.Single(nullable: false),
                        Humidity = c.Single(nullable: false),
                        Pressure = c.Single(nullable: false),
                        NO = c.Single(nullable: false),
                        NO2 = c.Single(nullable: false),
                        CO2 = c.Single(nullable: false),
                        LocalID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.MeteorologyID)
                .ForeignKey("dbo.Locals", t => t.LocalID, cascadeDelete: true)
                .Index(t => t.LocalID);

            CreateTable(
               "dbo.Hashtags",
               c => new
               {
                   HashtagID = c.Int(nullable: false, identity: true),
                   Name = c.String(),
               })
               .PrimaryKey(t => t.HashtagID);

            CreateTable(
                "dbo.POIs",
                c => new
                    {
                        POIID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Description = c.String(),
                        LocalID = c.Int(nullable: false),
                        UserId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.POIID)
                .ForeignKey("dbo.Locals", t => t.LocalID, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.LocalID)
                .Index(t => t.UserId);
                AddColumn("dbo.POIs", "tempoVisita", c => c.Int(nullable: false));
                AddColumn("dbo.POIs", "accessivel", c => c.Boolean(nullable: false));
                AddColumn("dbo.POIs", "validado", c => c.Boolean(nullable: false));

            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.POIs", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.POIs", "LocalID", "dbo.Locals");
            DropForeignKey("dbo.Meteorologies", "LocalID", "dbo.Locals");
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.POIs", new[] { "UserId" });
            DropIndex("dbo.POIs", new[] { "LocalID" });
            DropIndex("dbo.Meteorologies", new[] { "LocalID" });
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.POIs");
            DropTable("dbo.Meteorologies");
            DropTable("dbo.Locals");
            DropColumn("dbo.POIs", "validado");
            DropColumn("dbo.POIs", "accessivel");
            DropColumn("dbo.POIs", "tempoVisita");
            DropTable("dbo.Hashtags");
        }
    }
}
