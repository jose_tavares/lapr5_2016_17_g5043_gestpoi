﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLibrary.Models
{
    public class Meteorology
    {
        public int MeteorologyID { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Time of Reading")]
        public DateTime TimeOfReading { get; set; }
        [DataType(DataType.Time)]
        [DisplayFormat(DataFormatString = "{0:HH:mm:ss}", ApplyFormatInEditMode = true)]
        [Display(Name = "Hour of Reading")]
        public DateTime HourOfReading { get; set; }
        public float Temperature { get; set; }
        public float Wind { get; set; }
        public float Humidity { get; set; }
        public float Pressure { get; set; }
        public float NO { get; set; }
        public float NO2 { get; set; }
        public float CO2 { get; set; }
        public int LocalID { get; set; }
        public virtual Local Local { get; set; }
    }
}
