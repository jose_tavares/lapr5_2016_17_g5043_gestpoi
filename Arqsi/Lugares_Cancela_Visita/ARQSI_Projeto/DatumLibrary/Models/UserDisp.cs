﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLibrary.Models
{
    public class UserDisp
    {
        public int userDispID { get; set; }
        public string userID { get; set; }
        public virtual List<Disponibilidade> dias { get; set; }
        public Boolean acessivel { get; set; }
    }
}
