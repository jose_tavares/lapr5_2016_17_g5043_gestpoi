﻿using DataAccessLibrary.Infrastructure;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLibrary.Models
{
    [Table("Agenda")]
    public class Agenda
    {
        public int AgendaID { get; set; }
        public string Nome { get; set; }
        public string UserId { get; set; }
        [ForeignKey("UserId")]
        [Editable(false)]
        public virtual ApplicationUser User { get; set; }
        public int PercursoID { get; set; }
        public virtual List<Visita> lVisitas { get; set; }
    }
}
