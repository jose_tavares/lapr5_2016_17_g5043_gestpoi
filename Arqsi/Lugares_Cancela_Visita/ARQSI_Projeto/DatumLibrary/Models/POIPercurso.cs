﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLibrary.Models
{
    [Table("POIPercurso")]
    public class POIPercurso
    {
        [Key]
        public int POIPercursoID { get; set; }
        public virtual POI POI { get; set; }
        public virtual Percurso Percurso { get; set; }
    }
}
