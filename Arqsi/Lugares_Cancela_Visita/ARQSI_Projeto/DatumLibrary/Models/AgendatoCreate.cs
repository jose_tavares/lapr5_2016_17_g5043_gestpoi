﻿using DataAccessLibrary.DAL;
using DataAccessLibrary.Infrastructure;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;

namespace DataAccessLibrary.Models
{
    public class AgendatoCreate
    {
        private DatumDbContext db = new DatumDbContext();
        public List<POI> pois;
        public string userID;
        public List<Disponibilidade> ld;
        public Boolean useracessivel;
        public int userkm;
        public int userpe;
        public int usercarro;
        public int uservisita;

        public AgendatoCreate() {}

        public AgendatoCreate(Agenda a, List<Disponibilidade> ld)
        {
            Percurso pe = new Percurso();

            foreach(Percurso lp in db.Percursos.ToList())
            {
                if (a.PercursoID == lp.percursoID)
                {
                    pe = lp;
                }
            }
            UserDisp ud = new UserDisp();
            foreach(UserDisp uds in db.userDisps.ToList())
            {
                if (uds.userID == a.UserId)
                {
                    ud = uds;
                }
            }
            useracessivel = ud.acessivel;
            userkm = pe.kmTotal;
            userpe = pe.tempoPe;
            usercarro = pe.tempoCarro;
            uservisita = pe.tempoViagem;
            List<POI> p = new List<POI>();
            foreach(POIPercurso pp in db.POIPercursos.ToList())
            {
                if (a.PercursoID == pp.Percurso.percursoID)
                {
                    p.Add(pp.POI);
                }
            }
            this.pois = p;
            userID = a.UserId;
            this.ld = ld;
        }
    }
}
