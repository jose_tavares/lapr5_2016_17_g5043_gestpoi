﻿using DataAccessLibrary.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLibrary.Models
{
    public class PercursoToSend
    {
        public string Username;
        public int percursoID { get; set; }
        public string Name { get; set; }
        public int tempoPe { get; set; }
        public int tempoCarro { get; set; }
        public int kmTotal { get; set; }
        public int tempoPlaneado { get; set; }
        public int tempoViagem { get; set; }
        public Boolean predefinido { get; set; }

        public PercursoToSend() { }
        public PercursoToSend(Percurso p)
        {
            this.percursoID = p.percursoID;
            this.Name = p.Name;
            this.Username = p.User.UserName;
            this.tempoPe = p.tempoPe;
            this.tempoCarro = p.tempoCarro;
            this.kmTotal = p.kmTotal;
            this.tempoPlaneado = p.tempoPlaneado;
            this.tempoViagem = p.tempoViagem;
            this.predefinido = p.predefinido;
        }
    }
}
