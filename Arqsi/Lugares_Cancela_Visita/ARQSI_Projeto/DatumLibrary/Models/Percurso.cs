﻿using DataAccessLibrary.Infrastructure;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLibrary.Models
{
   public class Percurso
    {
        //ID, ID_Turista, Lista de POI, tempoPe, tempoCarro, kmTotal, tempoPLaneado, tempoViagem
        public int percursoID { get; set; }
        public string UserId { get; set; }
        [ForeignKey("UserId")]
        [Editable(false)]
        public virtual ApplicationUser User { get; set; }
        public virtual List<POI> listaPoi { get; set; }
        public string Name { get; set; }
        public int tempoPe { get; set; }
        public int tempoCarro { get; set; }
        public int kmTotal { get; set; }
        public int tempoPlaneado { get; set; }
        public int tempoViagem { get; set; }
        public Boolean predefinido { get; set; }

        public Percurso()
        {
        }

        public bool CheckNotOwner(string userId)
        {
            return userId != this.UserId;
        }
    }
}
