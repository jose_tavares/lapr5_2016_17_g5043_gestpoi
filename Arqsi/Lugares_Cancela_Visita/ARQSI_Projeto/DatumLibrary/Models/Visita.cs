﻿using DataAccessLibrary.Infrastructure;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLibrary.Models
{
    [Table("Visita")]
    public class Visita
    {
        public int VisitaID { get; set; }
        public string UserId { get; set; }
        [ForeignKey("UserId")]
        [Editable(false)]
        public virtual ApplicationUser User { get; set; }
        public POI poi;
        public Dias dia;
        public int hora;
        public Visita() { }
    }
}
