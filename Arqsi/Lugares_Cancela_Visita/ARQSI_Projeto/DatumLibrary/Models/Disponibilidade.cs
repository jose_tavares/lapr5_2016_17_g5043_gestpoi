﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLibrary.Models
{
    public enum Dias { Segunda = 2, Terça, Quarta, Quinta, Sexta, Sabado, Domingo };
    public enum Tempos { Manha, Tarde, Noite };

    [Table("Disponibilidade")]
    public class Disponibilidade
    {
        

        [Key]
        public int DisponibilidadeID { get; set; }
        public Dias Day { get; set; } 
        public Tempos TimeOfDay { get; set; } 
        public Disponibilidade() { }
    }
}
