﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLibrary.Models
{

    public class PoiDtoAbstract
    {
        public int POIID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public virtual List<Hashtag> hashtags { get; set; }
        public int tempoVisita { get; set; }
        public Boolean accessivel { get; set; }
        public virtual List<Disponibilidade> dias { get; set; }
        public Boolean validado { get; set; }
    }
}
