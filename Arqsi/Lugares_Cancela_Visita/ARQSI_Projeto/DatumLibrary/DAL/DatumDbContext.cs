﻿using DataAccessLibrary.Infrastructure;
using DataAccessLibrary.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static DataAccessLibrary.Infrastructure.ApplicationUser;

namespace DataAccessLibrary.DAL
{
    public class DatumDbContext : IdentityDbContext<ApplicationUser>
    {
        public DatumDbContext() : base("Datum") {
        }
        public DbSet<POI> POIs { get; set; }
        public DbSet<Local> Locals { get; set; }
        public DbSet<Meteorology> Meteorologias { get; set; }
        public DbSet<Hashtag> Hashtags { get; set; }
        public DbSet<Disponibilidade> Disponibilidades { get; set; }
        public DbSet<Percurso> Percursos { get; set; }
        public DbSet<POIPercurso> POIPercursos { get; set; }
        public DbSet<Agenda> Agendas { get; set; }
        public DbSet<Visita> Visitas { get; set; }
        public DbSet<UserDisp> userDisps { get; set; }

        //protected override void OnModelCreating(DbModelBuilder modelBuilder)
        //{
        //    base.OnModelCreating(modelBuilder);
        //}

        public static DatumDbContext Create()
        {
            return new DatumDbContext();
        }
    }
}
